"use strict";
import { createConnection } from 'mysql';

//local mysql db connection
var connection = createConnection({
    host     : 'uself.cbqkmqs6uabd.ap-southeast-1.rds.amazonaws.com',
    port     : '3306',
    user     : 'uselfadmin',
    password : 'uselfadmin',
    database : 'uself'
    // host     : process.env.RDS_HOSTNAME,
    // port     : process.env.RDS_PORT,
    // user     : process.env.RDS_USERNAME,
    // password : process.env.RDS_PASSWORD,
    // database : process.env.RDS_DB_NAME


});

connection.connect(function(err) {
    if (err){
        process.env['msg'] = "Unable to connect to RDS" + err;
        throw err;

    } else{
        console.log("MySql database is connected.");
        process.env['msg'] = "Success! Connected to RDS via" + process.env.RDS_HOSTNAME

    }

});

export default connection;
