/***************************
 * APOSTILLE BADGE
 ***************************/

export const API_URL_BADGR = `https://api.badgr.io`;
export const API_URL_DIGIBADGE = `http://api.u-self.com`;
//export const API_URL_DIGIBADGE = `http://localhost:3000`

/* To get token */
export const AUTHENTICATION = `${API_URL_BADGR}/o/token`;

/* Issue an assertion to single recipient */
export const POST_ASSERTION = (id) => `${API_URL_BADGR}/v2/badgeclasses/${id}/assertions`;

export const DELETE_ASSERTION = `${API_URL_BADGR}/v2/assertions/`

/* create an issuer or get list of issuers*/
export const ISSUER = `${API_URL_BADGR}/v2/issuers`

/* create a badge */
export const BADGECLASSES = `${API_URL_BADGR}/v2/badgeclasses`

/**get digibadge token */
export const DIGIBADGE_AUTHENTICATION = `${API_URL_DIGIBADGE}/digibadge/auth`
export const DIGIBADGE_RECIPIENT = `${API_URL_DIGIBADGE}/digibadge/recipient`