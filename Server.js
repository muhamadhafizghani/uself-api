"use strict";
import route from './src/route';
import express from 'express';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import cors from 'cors'

//const express = require('express'),
const app = express(),
//bodyParser = require('body-parser');
port = process.env.PORT || 3000;


app.listen(port);

console.log('API server started on: ' + port);
// enable files upload
app.use(fileUpload({
  createParentPath: true
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(cors({origin: true, credentials: true}));

// Add headers
app.use(function(req, res, next) {
    // Website you wish to allow to connect
    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
    res.setHeader("Access-Control-Allow-Origin", "https://u-self.com");
  
    // Request methods you wish to allow
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );
  
    // Request headers you wish to allow
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, Content-Type, Authorization"
    );
  
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);
  
    // Pass to next layer of middleware
    next();
  });

//var routes = require('./src/route'); //importing route
route(app); //register the route



