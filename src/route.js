"use strict";
import {write_a_user, read_all_users,update_user, read_a_user, delete_a_user} from './uself/auth/controllers/userController';
import { user_login} from './uself/auth/controllers/authController';
import{ write_an_enrollment,read_enrollment,read_enrollment_by_course_id,update_status_inprogress } from './uself/enrollment/controllers/enrollController';
import { read_courses_by_title, read_course_requirement_by_id,read_courses } from './uself/enrollment/controllers/courseController';
import { update_answer, read_user_mark } from './uself/learning/controllers/userMarkController'
import { read_modules_by_course_id,read_questions_by_course_id,read_answer_by_question_id } from './uself/learning/controllers/learningController'
import { write_badge_user, read_badge_user } from './uself/ebadge/controllers/badgeController'

import {apostille_user_login} from './apostille/auth/controllers/apostilleAuthController';
import {write_an_apostille_user,read_an_apostille_user,read_all_apostille_users,delete_an_apostille_user} from './apostille/auth/controllers/apostilleUserController';
import{write_apostille_ebadge_issuance} from './apostille/badge_issuance/controller/badgeIssuanceController';
import{read_apostille_ebadge_recipient_by_badge_id,read_apostille_ebadge_recipients} from './apostille/badge_issuance/controller/badgeRecipientController';
import{ write_apostille_badge,read_apostille_badges,read_apostille_badge_by_id } from './apostille/badge_recognition/controllers/badgeRecognitionController'
import auth from './utils/middleware/auth';
import authApostille from './utils/middleware/authApostille';

export default function(app) {
    

    /************************************
     * AUTH
     ************************************/

    // user Routes
    app.route('/users')
      //.get(read_all_users)
      .post(write_a_user)
      .get([auth],read_a_user)
      .put([auth],update_user)
      //.delete([auth],delete_a_user)

    //auth Routes get token
    app.route('/auth')
      .post(user_login);


    /************************************
     * ENROLLMENT
     ************************************/

    //enroll Routes - return course and requirements for get
    app.route('/enroll')
      .post([auth],write_an_enrollment)
      .get([auth],read_enrollment);
    app.route('/enroll/:courseId')
      .get([auth],read_enrollment_by_course_id);
    //enroll inprogress
    app.route('/enroll/status')
      .put([auth],update_status_inprogress)
  
    //course search route
    app.route('/courses/:title')
      .get(read_courses_by_title);

    app.route('/courses')
      .get(read_courses);

    //course requirement
    app.route('/courses/requirements/:courseId')  
      .get(read_course_requirement_by_id);


    /************************************
     * LEARNING
     ************************************/

    // //validate answer [OLD:NEED TO BE UPDATED]
    // app.route('/validate/answers')
    //   .post([auth],write_a_mark)
    // //user answer [OLD:DEPRECATED]
    // app.route('/user/answers/:courseId')
    //   .get([auth],read_user_mark)


    //user answer admin only
    app.route('/answers/:questionId')
    .get([auth],read_answer_by_question_id)

    //user answer (update) the question
    app.route('/user/answers/:questionId')
    .put([auth],update_answer)

    //learning route
    app.route('/modules/:courseId')
      .get([auth], read_modules_by_course_id);

    //learning route
    app.route('/questions/:courseId')
      .get([auth], read_questions_by_course_id);

    /************************************
     * E-BADGE
     ************************************/

    //write and read badge
    app.route('/badges')
      .post([auth],write_badge_user)
      .get([auth],read_badge_user)




    /************************************
     * APOSTILLE AUTH
     ************************************/

    // user Routes
    /**
     * form-data
     *  
     * key:data
     * value:{"company_name":"Multimedia University",
     *  "company_registration_number":"436821-T",
     *  "email":"cqaae@mmu.edu.my",
     *  "password":"qwER_1234",
     *  "phone":"0383125149",
     *  "date_start_operate":"27-05-2000",
     *  "url":"http://www.mmu.edu.my"}
     * content-type : application/json 
     * key:image value: 'Select Image' content-type: auto
     */
    app.route('/digibadge/users')
      //.get(read_all_users)
      .post(write_an_apostille_user)
      .get([authApostille],read_an_apostille_user)
      //.delete([auth],delete_a_user)

    //auth Routes get token
    app.route('/digibadge/auth')
      .post(apostille_user_login);

    /************************************
     * APOSTILLE BADGE ISSUANCE
     ************************************/
    
    //issue and read (badge_id) e-badge
    /**
     * Body 
     * content-type : application/json
     * 
     * {
     *    "recipient_email":"fizg_spek34@yahoo.com",
     *     "recipient_name" : "Muhamad Hafiz Ghani",
     *     "description" : "Pass the test with 45",
     *     "badgeclass_id" : "V94SUA64TOOQgHJ1xvxslg",
     *     "url_evidence" :"https://uself.com"
     *   } 
     */
    app.route('/digibadge/recipient')
    .post([authApostille],write_apostille_ebadge_issuance)
    .get([authApostille],read_apostille_ebadge_recipient_by_badge_id)

    //issue and read (assertion_id) e-badge
    app.route('/digibadge/recipient/:assertionId')
    .get(read_apostille_ebadge_recipients)
    
    /************************************
     * APOSTILLE BADGE RECOGNITION
     ************************************/

    /**
    * form-data 
    *  
    * key:data value:{"badge_description":"Node Js awarded to recipient enroll node js course","badge_name":"Node JS Basic","criteria":"more than 50"}
    * content-type: application/json
    * key:image value: 'Select Image' content-type: auto
    */
    app.route('/digibadge/badges')
    .post([authApostille],write_apostille_badge)
    .get([authApostille],read_apostille_badges)

    app.route('/digibadge/badges/:badgeId')
    .get([authApostille],read_apostille_badge_by_id)



};