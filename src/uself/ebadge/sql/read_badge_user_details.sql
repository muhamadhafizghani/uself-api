SELECT 
    CONCAT(U.first_name, ' ', U.last_name) as name,
    U.email,
    B.id,
    B.badge_description,
    B.badge_level,
    E.status,
    (E.user_total_mark/C.total_mark * 100) AS result
FROM 
    enrollment E
LEFT JOIN user U on E.user_id = U.id
LEFT JOIN course C ON E.course_id = C.id
LEFT JOIN badge B ON E.course_id = B.course_id
WHERE
    E.user_id = ? AND
    E.course_id = ? AND
    B.badge_level = (CASE 
        WHEN (E.user_total_mark/C.total_mark * 100) >= 80 THEN "EXPERT"
        WHEN (E.user_total_mark/C.total_mark * 100) >= 65 AND (E.user_total_mark/C.total_mark * 100) <= 79 THEN "ADVANCED"
        WHEN (E.user_total_mark/C.total_mark * 100) >= 50 AND (E.user_total_mark/C.total_mark * 100) <= 64 THEN "INTERMEDIATE"
        WHEN (E.user_total_mark/C.total_mark * 100) <= 49 THEN "BEGINNER"
    END)
