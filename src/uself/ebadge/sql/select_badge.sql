SELECT 
    C.course_name,
    U.badge_id,
    U.badge_url,
    U.badge_expired,
    U.badge_issue,
    U.badge_level,
    U.result,
    U.qr_code,
    U.assertion_id,
    U.hash,
    B.badge_name,
    B.badge_description
FROM 
    badge_user U
LEFT JOIN badge B ON U.badge_id = B.id
LEFT JOIN course C ON B.course_id = C.id 
WHERE 
    U.user_id = ?
