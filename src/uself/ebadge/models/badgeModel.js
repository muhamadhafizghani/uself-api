import connection from '../../../../config/db';
import {removeNull} from '../../../utils/removeNull';
import fs from 'fs';
import path from 'path';

export class Badge{
    constructor(badge){

        this.badge_id = badge.badge_id;
        this.user_id = badge.user_id;
        this.assertion_id = badge.assertion_id;
        this.badge_url = badge.badge_url;
        this.badge_expired = badge.badge_expired;
        this.badge_issue = badge.badge_issue;
        this.badge_level = badge.badge_level;
        this.qr_code = badge.qr_code;
        this.result = badge.result;
        this.hash = badge.hash;

    }

}
export default Badge;