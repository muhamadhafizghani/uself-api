import connection from '../../../../config/db';
import {removeNull} from '../../../utils/removeNull';
import fs from 'fs';
import path from 'path';


export class BadgeUser{
    constructor(badge){

        this.badge_id = badge.badge_id;
        this.user_id = badge.user_id;
        this.assertion_id = badge.assertion_id;
        this.badge_url = badge.badge_url;
        this.badge_expired = badge.badge_expired;
        this.badge_issue = badge.badge_issue;
        this.badge_level = badge.badge_level;
        this.qr_code = badge.qr_code;
        this.result = badge.result;
        this.hash = badge.hash;

    }

    static writeBadgeUser(badge, result){

        removeNull(badge)
        try {
            connection.query("INSERT INTO badge_user set ?", badge, function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }else {
                    result(null, res);
                }

            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }
    }

    static readBadgeUser(user_id, result){

        try {
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_badge.sql')).toString();
            connection.query(sql, user_id, function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else {
                    //console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }

    }

    static readDetails(user_id,course_id, result){

        try {
            var param = [
                user_id,
                course_id
            ]
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/read_badge_user_details.sql')).toString();
            connection.query(sql, param, function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else {
                    //console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }

    }



}
export default BadgeUser;