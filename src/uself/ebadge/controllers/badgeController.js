import BadgeUser from '../models/badgeUserModel';
import axios from 'axios';
import {digiBadgeAuth} from '../../../utils/middleware/digiBadgeAuth';
import {DIGIBADGE_RECIPIENT} from '../../../../config/config';
import {isEmpty} from '../../../utils/index'

export function write_badge_user(req,res){

    var badge_user = new BadgeUser(req.body);
    badge_user.user_id = req.body.user_id;
    var course_id = req.body.course_id;

    if (req.user.role !== "SYSTEM"){

        res.status(403).send({ error:true, message: "User are not authorized to perform this action." });

    }else if(!course_id){

        res.status(400).send({ error:true, message: 'Please provide course id' });
        
    }else if(!badge_user.user_id){

        res.status(400).send({ error:true, message: 'Please provide user id' });
        
    }else{



        BadgeUser.readDetails(badge_user.user_id,course_id,async function(err,result){

            if(err){
                res.status(400).send({ error:true, message: 'Please contact support@u-self.com' });

            }else if(isEmpty(result)){

                res.status(400).send({ error:true, message: 'User did not complete the assessment or did not enroll the course' });
            
            }else if(result[0].status !== 'COMPLETE'){

                res.status(400).send({ error:true, message: 'User must complete the assessment' });
                
            }else{
                //data assign
                badge_user.badge_id = result[0].id
                var name = result[0].name
                var email = result[0].email
                var description = result[0].badge_description
                badge_user.badge_level = result[0].badge_level
                badge_user.result = result[0].result

                var auth_digibadge_token = null;

                //get token
                digiBadgeAuth().then((response)=>{

                    //post assertion
                    try {

                        //set token
                        auth_digibadge_token = response.token
                        const header = {
                            headers: {
                                "Content-Type": "application/json",
                                'Authorization': "bearer " + auth_digibadge_token
                            }
                        };

                        //set request body
                        const body = JSON.stringify({

                            "recipient_name": name,
                            "recipient_email": email,
                            "badgeclass_id": badge_user.badge_id,
                            "description": description,
                            "url_evidence": "http://u-self.com"
                            
                        });
                        
                        //send request
                        return axios.post(DIGIBADGE_RECIPIENT, body, header);



                    }catch(err){
                        console.log("response failed")
                        console.log(err)
                        res.status(400).send({error:true, message:"Please contact support@u-self.com"})
                    }


                }, (err) =>{
                    console.log(err)
                    res.status(400).send({error:true, message:"Please contact support@u-self.com"})

                }).then((response) => {

                    badge_user.hash = response.data.transactionHash;
                    badge_user.badge_url = response.data.apostille_path;
                    badge_user.assertion_id = response.data.assertion_id;
                    badge_user.qr_code = response.data.qr_code;
                    badge_user.badge_issue = response.data.time_created;


                    BadgeUser.writeBadgeUser(badge_user, function(err,result){

                        if (err){

                            if(err.sqlMessage.toLowerCase().includes("duplicate entry") ){
                                console.log(err)
                                res.status(400).send({ error:true, message: "The badge is already assigned to the user"});
                            }else{
                                console.log(err)
                                res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
                            }

                        }else{

                            res.json(badge_user);

                        }

                    })


                }, (err) =>{

                    console.log(err)
                    res.status(400).send({error:true, message:"Please contact support@u-self.com"})
                })
            }

           

        })

    }//end if

}

export function read_badge_user(req,res){
    const user_id = req.user.id;
    
    BadgeUser.readBadgeUser(user_id, function(err,result){
        
        if (err){
            console.log(err)
            res.status(400).send({error:true, message:"Please contact support@u-self.com"})
        }else{
            res.send(result)

        }
    });

}