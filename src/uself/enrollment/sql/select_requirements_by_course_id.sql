SELECT
    R.id,
    R.description,
    skill_level
FROM 
    course_requirement C LEFT JOIN
    requirement R
ON 
    C.requirement_id = R.id
WHERE 
    C.course_id = ?