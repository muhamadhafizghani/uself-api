SELECT 
    C.id,
    C.course_name,
    C.price,
    C.description,
    C.media,
    C.total_mark,
    C.image,
    C.language,
    C.rating,
    C.skill_level,
    C.user_id           AS instructor_id,
    U.first_name        AS instructor_first_name,
    U.last_name         AS instructor_last_name,
    U.email             AS instructor_email,
    U.phone             AS instructor_phone,
    CR.skill_level      AS req_skill_level,
    R.description       AS requirement

FROM 
    course C
LEFT JOIN user U ON U.id = C.user_id
LEFT JOIN course_requirement CR ON CR.course_id = C.id 
LEFT JOIN requirement R ON CR.requirement_id = R.id
    