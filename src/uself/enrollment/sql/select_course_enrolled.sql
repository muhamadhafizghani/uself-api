SELECT 
    E.course_id,
    status,
    E.rating              AS my_rating,
    E.competency,
    E.time_created,
    E.user_total_mark,
    C.course_name,
    C.total_mark,
    C.price,
    C.description,
    C.media,
    C.image,
    C.language,
    C.rating,
    C.skill_level,
    C.user_id           AS instructor_id,
    U.first_name        AS instructor_first_name,
    U.last_name         AS instructor_last_name,
    U.email             AS instructor_email,
    U.phone             AS instructor_phone,
    CR.skill_level      AS req_skill_level,
    R.description       AS requirement
FROM 
    enrollment E   
LEFT JOIN course C ON E.course_id = C.id
LEFT JOIN user U ON U.id = C.user_id
LEFT JOIN course_requirement CR ON CR.course_id = C.id 
LEFT JOIN requirement R ON CR.requirement_id = R.id
WHERE 
    E.user_id = ?
    
