"use strict";
import connection from '../../../../config/db';
import fs from 'fs';
import path from 'path';

export class Course{

    //Course object constructor
    constructor(course){

        this.course_name = course.course_name;
        this.price = course.price;
        this.total_mark = course.total_mark;
        this.description = course.description;
        this.media = course.media;
        this.image = course.image;
        this.language = course.language;
        this.rating = course.rating;
        this.skill_level = course.skill_level;
        this.user_id = course.user_id;

    }

    static readCourseById(id, result){

        try {
            connection.query("SELECT * FROM course WHERE id = ?", id, function (err, res) {
                if (err) {
                    //console.log("error: ", err);
                     result(err, null);
                 }
                 else {
                     result(null, res);
                 }
            });
        }
        catch (err) {
            //console.log(err);
            result(err, null);
        }

    }


    static readCourses(result){
        var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_courses.sql')).toString();

        connection.query(sql, function (err, res) {
            if (err) {
                //console.log("error: ", err);
                result(null, err);
            }
            else {
                //console.log('user : ', res);
                result(null, res);
            }
        });

    }

    static readCourseByCourseName(course_name,result){

        var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_courses.sql')).toString();
        connection.query(sql + "WHERE course_name like ? ", course_name, function (err, res) {
            if (err) {
                //console.log("error: ", err);
                result(null, err);
            }
            else {
                //console.log('user : ', res);
                result(null, res);
            }
        });

    }





}
export default Course;