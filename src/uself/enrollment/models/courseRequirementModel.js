import connection from '../../../../config/db'
import fs from 'fs';
import path from 'path';

export class CourseRequirement {

    //Course requirement object constructor
    constructor(course_requirement) {

        this.course_id = course_requirement.course_id;
        this.requirement_id = course_requirement.requirement_id;
        this.skill_level = course_requirement.skill_level;
        this.description = course_requirement.description;

    
    }

    static readCourseRequirementsByCourseId(course_id,result){
        try {
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_requirements_by_course_id.sql')).toString();
            connection.query(sql, course_id, function (err, res) {
                if (err) {
                    //console.log("error: ", err);
                    result(err, null);
                }
                else {
                    //console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            // console.log(err);
            result(err, null);
        }
    }




}

export default CourseRequirement;