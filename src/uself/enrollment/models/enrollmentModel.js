import connection from '../../../../config/db';
import fs from 'fs';
import path from 'path';
import {removeNull} from '../../../utils/removeNull';

export class Enrollment {
    
    constructor(enrollment){

        this.user_id = enrollment.user_id;
        this.course_id = enrollment.course_id;
        this.status = enrollment.status;
        this.rating = enrollment.rating;
        this.user_total_mark = enrollment.user_total_mark;
        this.competency = enrollment.competency;
        this.time_created = enrollment.time_created;
    }


    static writeEnrollment(enrollment, result){
        
        try{
            //remove the null object so the default will be wrote in the database
            removeNull(enrollment)

            connection.query("INSERT INTO enrollment set ?", enrollment, function(err,res){
                if (err) {
                    console.log("error: ", err.sqlMessage);
                    result(err, null);
                }
                else {
                    //console.log(res);
                    result(null, res);
                }
            });

        }catch(err){

            result(err, null);

        }

    }

    static readCourseEnrolledByUserId(userId,result){

        try {

            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_course_enrolled.sql')).toString();
            connection.query(sql, userId, function (err, res) {
                if (err) {
                    //console.log("error: ", err);
                    result(err, null);
                }
                else {
                    //console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            // console.log(err);
            result(err, null);
        }
    }

    static updateStatusInProgress(userId,courseId,result){
        try {

            let param = [   
                userId,
                courseId
            ]
            connection.query('UPDATE enrollment SET status = "IN-PROGRESS" WHERE user_id = ? AND course_id = ? AND status = "NEW"', param, function (err, res) {
                if (err) {
                    //console.log("error: ", err);
                    result(err, null);
                }
                else {
                    //console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            // console.log(err);
            result(err, null);
        }
    }


    static readCourseEnrolledByUserIdAndCourseId(userId,courseId,result){

        try {

            var param = [
                userId,
                courseId
            ];
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_course_enrolled.sql')).toString();
            connection.query(`${sql} AND E.course_id = ?`, param, function (err, res) {
                if (err) {
                    //console.log("error: ", err);
                    result(err, null);
                }
                else {
                    //console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            // console.log(err);
            result(err, null);
        }
    }


} 
export default Enrollment;