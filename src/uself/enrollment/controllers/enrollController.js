"use strict";
import Enrollment from '../models/enrollmentModel';

//user enroll the course
export function write_an_enrollment(req, res){
    var new_enrollment = new Enrollment(req.body);
    new_enrollment.user_id = req.user.id;

    if(!new_enrollment.course_id){
        res.status(400).send({ error:true, message: 'Please provide course id' });
    }else{
        Enrollment.writeEnrollment(new_enrollment,function(err, result) {
            
            if (err){
                if(err.sqlMessage.includes("Duplicate entry") && err.sqlMessage.toUpperCase().includes("PRIMARY") ){
                    res.status(400).send({ error:true, message: "User already enrolled the course"});
                }else if(err.sqlMessage.includes("foreign key constraint")){
                    res.status(400).send({ error:true, message: "Invalid course id. Course does not exist"});
                }else{
                    res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
                }
            
            }else{
                res.json(result);
            }

        });
    }
}

//user's enrolled course
export function read_enrollment(req, res){
    Enrollment.readCourseEnrolledByUserId(req.user.id, function(err, result){
        if (err)

            res.status(400).send({ error:true, message: err.sqlMessage });

        else{

            var courses = [];
                
            var course = {};
            var prevId = null;
    
            var requirements = [];
    
    
            
            result.forEach((element,index) => {
                
                if (element.course_id !== prevId){
                    //to skip first case
                    if(prevId != null){
    
                        //push data into array
                        course.requirements = requirements;
                        courses.push(course);
    
                        //reset course object and requirements array
                        course ={};
                        requirements = [];
                    }
                    
                    //set course
                    course.course_id = element.course_id;
                    course.status = element.status;
                    course.my_rating = element.my_rating;
                    course.competency = element.competency;
                    course.time_created = element.time_created;
                    course.user_total_mark = element.user_total_mark;
                    course.total_mark = element.total_mark;
                    course.course_name = element.course_name;
                    course.image = element.image;
                    course.price = element.price;
                    course.description = element.description;
                    course.media = element.media;
                    course.language = element.language;
                    course.rating = element.rating;
                    course.skill_level = element.skill_level;
                    course.instructor_id = element.instructor_id;
                    course.instructor_first_name = element.instructor_first_name;
                    course.instructor_last_name = element.instructor_last_name;
                    course.instructor_email = element.instructor_email;
                    course.instructor_phone = element.instructor_phone;


                    prevId = element.course_id;
                    
                }
    
                //set requirement
                if (element.requirement !== null){
                    
                    var requirement = {};
        
                    requirement.description = element.requirement;
                    requirement.skill_level = element.req_skill_level;

        
                    requirements.push(requirement);
                }
                
                //for last case
                if (Object.is(result.length - 1, index)) {
                 
    
                    //push data into array
                    course.requirements = requirements;
                    courses.push(course);

                    //reset course object and requirements array
                    course ={};
                    requirements = [];
    
                }
    
            
            });

            res.json(courses);


        }
    })
}

export function read_enrollment_by_course_id(req, res){
    Enrollment.readCourseEnrolledByUserIdAndCourseId(req.user.id,req.params.courseId, function(err, result){
        if (err)

            res.status(400).send({ error:true, message: err.sqlMessage });

        else{

            var courses = [];
                
            var course = {};
            var prevId = null;
    
            var requirements = [];
    
    
            
            result.forEach((element,index) => {
                
                if (element.course_id !== prevId){
                    //to skip first case
                    if(prevId != null){
    
                        //push data into array
                        course.requirements = requirements;
                        courses.push(course);
    
                        //reset course object and requirements array
                        course ={};
                        requirements = [];
                    }
                    
                    //set course
                    course.course_id = element.course_id;
                    course.status = element.status;
                    course.my_rating = element.my_rating;
                    course.competency = element.competency;
                    course.time_created = element.time_created;
                    course.user_total_mark = element.user_total_mark;
                    course.total_mark = element.total_mark;
                    course.course_name = element.course_name;
                    course.image = element.image;
                    course.price = element.price;
                    course.description = element.description;
                    course.media = element.media;
                    course.language = element.language;
                    course.rating = element.rating;
                    course.skill_level = element.skill_level;
                    course.instructor_id = element.instructor_id;
                    course.instructor_first_name = element.instructor_first_name;
                    course.instructor_last_name = element.instructor_last_name;
                    course.instructor_email = element.instructor_email;
                    course.instructor_phone = element.instructor_phone;


                    prevId = element.course_id;
                    
                }
    
                //set requirement
                if (element.requirement !== null){
                    
                    var requirement = {};
        
                    requirement.description = element.requirement;
                    requirement.skill_level = element.req_skill_level;

        
                    requirements.push(requirement);
                }
                
                //for last case
                if (Object.is(result.length - 1, index)) {
                 
    
                    //push data into array
                    course.requirements = requirements;
                    courses.push(course);

                    //reset course object and requirements array
                    course ={};
                    requirements = [];
    
                }
    
            
            });

            res.json(courses);


        }
    })
}



export function update_status_inprogress(req, res){
    var new_enrollment = new Enrollment(req.body);
    new_enrollment.user_id = req.user.id;

    if(!new_enrollment.course_id){
        res.status(400).send({ error:true, message: 'Please provide course id' });
    }else{
        Enrollment.updateStatusInProgress(new_enrollment.user_id,new_enrollment.course_id,function(err, result) {
            
            if (err){

                res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
        
            
            }else{
                res.status(200).send({ error:false, message: "Enrollment status updated"});
            }

        });
    }
}
