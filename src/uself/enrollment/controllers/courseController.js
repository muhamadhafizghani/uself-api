"use strict";
import Course from '../models/courseModel';
import CourseRequirement from '../models/courseRequirementModel';


export function read_courses_by_title(req, res){
    var title = "%" +req.params.title + "%";

    Course.readCourseByCourseName(title,function(err,result){

        if (err){

            res.status(400).send({ error:true, message: err.sqlMessage });

        }else{

            var courses = [];
            
            var course = {};
            var prevId = null;
    
            var requirements = [];
    
    
            
            result.forEach((element,index) => {
                
                if (element.id !== prevId){
                    //to skip first case
                    if(prevId != null){
    
                        //push data into array
                        course.requirements = requirements;
                        courses.push(course);
    
                        //reset course object and requirements array
                        course ={};
                        requirements = [];
                    }
                    
                    //set course
                    course.id = element.id;
                    course.course_name = element.course_name;
                    course.price = element.price;
                    course.description = element.description;
                    course.media = element.media;
                    course.image = element.image;
                    course.price = element.price;
                    course.total_mark = element.total_mark;
                    course.language = element.language;
                    course.rating = element.rating;
                    course.skill_level = element.skill_level;
                    course.instructor_id = element.instructor_id;
                    course.instructor_first_name = element.instructor_first_name;
                    course.instructor_last_name = element.instructor_last_name;
                    course.instructor_email = element.instructor_email;
                    course.instructor_phone = element.instructor_phone;
    
    
    
                    prevId = element.id;
                    
                }
    

                //set requirement
                if (element.requirement !== null){
                    var requirement = {};
    

                    requirement.description = element.requirement;
                    requirement.skill_level = element.req_skill_level;
                    requirements.push(requirement);

                }
    
                    
                //for last case
                if (Object.is(result.length - 1, index)) {
                    
    
                    //push data into array
                    course.requirements = requirements;
                    courses.push(course);
    
                    //reset course object and requirements array
                    course ={};
                    requirements = [];
    
                }
    
            
            });

        }

        res.json(courses);
            
    })
}

export function read_courses(req,res){

    Course.readCourses(function(err,result){

        if (err){

            res.status(400).send({ error:true, message: err.sqlMessage });

        }else{

            var courses = [];
            
            var course = {};
            var prevId = null;
    
            var requirements = [];
    
    
            
            result.forEach((element,index) => {
                
                if (element.id !== prevId){
                    //to skip first case
                    if(prevId != null){
    
                        //push data into array
                        course.requirements = requirements;
                        courses.push(course);
    
                        //reset course object and requirements array
                        course ={};
                        requirements = [];
                    }
                    
                    //set course
                    course.id = element.id;
                    course.course_name = element.course_name;
                    course.price = element.price;
                    course.description = element.description;
                    course.media = element.media;
                    course.total_mark = element.total_mark;
                    course.image = element.image;
                    course.price = element.price;
                    course.language = element.language;
                    course.rating = element.rating;
                    course.skill_level = element.skill_level;
                    course.instructor_id = element.instructor_id;
                    course.instructor_first_name = element.instructor_first_name;
                    course.instructor_last_name = element.instructor_last_name;
                    course.instructor_email = element.instructor_email;
                    course.instructor_phone = element.instructor_phone;
    
    
    
                    prevId = element.id;
                    
                }
    

                //set requirement
                if (element.requirement !== null){
                    var requirement = {};
    

                    requirement.description = element.requirement;
                    requirement.skill_level = element.req_skill_level;
                    requirements.push(requirement);

                }
    
                    
                //for last case
                if (Object.is(result.length - 1, index)) {
                    
    
                    //push data into array
                    course.requirements = requirements;
                    courses.push(course);
    
                    //reset course object and requirements array
                    course ={};
                    requirements = [];
    
                }
    
            
            });

        }

        res.json(courses);
            
    })
}


export function read_course_requirement_by_id(req,res){
    const courseId = req.params.courseId
    CourseRequirement.readCourseRequirementsByCourseId(courseId,function(err,result){
        if (err)
            res.status(400).send({ error:true, message: err.sqlMessage });
        else
            res.json(result);
       
    })
}