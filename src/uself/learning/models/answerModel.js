import connection from '../../../../config/db';
import fs from 'fs';
import path from 'path';

export class Answer{

    constructor(answer){
        this.id = answer.id;
        this.question_id = answer.question_id;
        this.mark =answer.mark;
        this.is_right = answer.is_right;
    }

    static readAnswer(questionId, result){

        try{

            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/read_answer.sql')).toString();
            connection.query(sql,questionId,function(err,res){
                
                if(err){
                    result(err, null);
                }else{
                    result(null, res);

                }

            });

        }catch(e){
            console.log(e)
            result(err, null);

        }

    }
}
export default Answer;