import connection from '../../../../config/db';
import fs from 'fs';
import path from 'path';

export class Module {

    constructor(module){
        this.id = module.id;
        this.module_name = module.module_name;
        this.course_id = module.course_id;
        this.module_description = module.module_description;
        this.module_number = module.module_number;
        this.media = module.media;

    }

    static readModules(userId,courseId, result){

        try{
            var param = [
                courseId,
                userId
            ]
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/read_modules.sql')).toString();
            connection.query(sql,param,function(err,res){
                
                if(err){
                    result(err, null);
                }else{
                    result(null, res);
                }

            })

        }catch(err){

            console.log(err);
            result(err, null);

        }

    }

}

export default Module;