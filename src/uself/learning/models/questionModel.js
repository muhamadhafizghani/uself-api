import connection from '../../../../config/db';
import fs from 'fs';
import path from 'path';

export class Question{

    constructor(question){
        this.id = question.id;
        this.course_id = question.course_id;
        this.question = question.question;
        this.question_number = question.question_number;
        this.media = question.media;
        this.mark = question.mark;

    }

    static readQuestionsByCourseId(courseId,userId,result){
        try{
            var param = [
                courseId,
                userId
            ]
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/read_question_user_mark.sql')).toString();
            connection.query(sql,param,function(err,res){

                if(err){
                    result(err,null)
                }else{
                    result(null,res)
                }

            })
        }catch(err){
            console.log(err);
            result(err,null);
        }
    }

    // static readModulesQuestionsByCourseId(courseId,result){

    //     try{

    //         
    //         connection.query(sql,courseId,function(err,res){
                
    //             if(err){
    //                 result(err, null);
    //             }else{
                   

    //                 //console.log(res);

    //                 result(null, res);
    //             }

    //         })

    //     }catch(err){

    //         console.log(err);
    //         result(err, null);

    //     }
    // }
}

export default Question;