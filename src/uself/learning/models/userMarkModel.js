import connection from '../../../../config/db';
import fs from 'fs';
import path from 'path';

export class UserMark{

    constructor(user_mark){
        this.user_id = user_mark.user_id;
        this.course_id = user_mark.course_id;
        this.module_id = user_mark.module_id;
        this.question_id = user_mark.question_id;
        this.mark = user_mark.mark;
        this.answer = user_mark.answer;
        this.right_answer = user_mark.right_answer;
        this.similarity = user_mark.similarity;
    }

    static writeAnswer(user_mark, result){

        try {
            connection.query("INSERT INTO user_mark set ?", user_mark, function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else {
                    console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }

    }

    static updateAnswer(user_mark,result){

        try {

            var param = [
                user_mark.mark,
                user_mark.answer,
                user_mark.similarity,
                user_mark.question_id,
                user_mark.user_id
            ]

            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/update_user_mark.sql')).toString();
            connection.query(sql, param, function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }

    }

    static readAnswer(user_mark,result){


        var filter = [user_mark.user_id , user_mark.course_id];

        try {
            connection.query("SELECT * FROM user_mark WHERE user_id = ? and course_id = ?", filter, function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }
    }

    // static updateUserAnswer(user_mark,result){
    //     var param = [user_mark.user_id , user_mark.course_id];

    //     try {
    //         connection.query("SELECT * FROM user_mark WHERE user_id = ? and course_id = ?", filter, function (err, res) {

    //             if (err) {
    //                 console.log("error: ", err);
    //                 result(err, null);
    //             }
    //             else {
    //                 result(null, res);
    //             }
    //         });
    //     }
    //     catch (err) {
    //         console.log(err);
    //         result(err, null);
    //     }
    // }



}

export default UserMark;