SELECT
    Q.mark,
    A.answer
FROM 
    question Q
LEFT JOIN
    answer A
ON
    Q.id = A.question_id
WHERE 
    A.is_right = 1 AND
    Q.id = ?

