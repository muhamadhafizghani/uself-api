UPDATE 
    user_mark 
SET 
    status = 'COMPLETE', 
    mark = ? ,
    answer = ?,
    similarity = ?
WHERE 
    question_id= ? AND 
    user_id = ? AND 
    status = 'NEW'