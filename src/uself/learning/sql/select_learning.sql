SELECT
    M.id,
    M.module_number,
    M.module_name,
    M.module_description,
    M.media,
    Q.question,
    Q.media,
    Q.mark,
    Q.id AS q_id,
    (SELECT COUNT(*) FROM answer WHERE question_id = q_id) AS answer_number

FROM 
    module M 
LEFT JOIN
    question Q 
ON
    M.id = Q.module_id
WHERE 
    M.course_id = ?
ORDER BY
    M.module_number


