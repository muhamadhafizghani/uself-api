SELECT 
    M.id,
    M.module_name,
    M.module_number,
    M.module_description,
    M.media,
    M.course_id
FROM 
    module M
LEFT JOIN
    enrollment E
ON
    M.course_id = E.course_id
WHERE 
    E.course_id = ? AND
    E.user_id = ?
ORDER BY
    M.module_number