SELECT
    M.mark AS user_mark,
    M.answer,
    M.status,
    Q.question_number,
    Q.question,
    Q.id,
    Q.media,
    Q.mark AS question_mark
FROM 
    user_mark M 
LEFT JOIN
    question Q 
ON
    M.question_id = Q.id
WHERE 
    M.course_id = ? AND
    M.user_id = ?
ORDER BY
    Q.question_number
