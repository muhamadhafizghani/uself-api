SELECT 
    Q.question,
    Q.course_id,
    Q.mark,
    A.answer
FROM 
    question Q 
LEFT JOIN
    answer A 
ON
    A.question_id = Q.id
WHERE
    A.is_right = 1 AND
    Q.id = ?