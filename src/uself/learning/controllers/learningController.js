import Question from '../models/questionModel';
import Module from '../models/moduleModel';
import Answer from '../models/answerModel';

export function read_modules_by_course_id(req,res){
    const courseId = req.params.courseId

    Module.readModules(req.user.id,courseId, function(err,result){
        if (err){
            console.log(err)
            res.status(400).send({ error:true, message: 'please contact support@u-self.com' });
        }
        res.json(result);
    })

}

export function read_questions_by_course_id(req,res){
    const courseId = req.params.courseId
    const userId = req.user.id

    Question.readQuestionsByCourseId(courseId,userId, function(err,result){
        if (err){
            console.log(err)
            res.status(400).send({ error:true, message: 'please contact support@u-self.com' });
        }
        res.json(result);
    })
}

export function read_answer_by_question_id(req,res){
    const questionId = req.params.questionId

    if (req.user.role !=="ADMIN"){

        res.status(403).send({ error:true, message: "User are not authorized to perform this action." });

    }else{
        Answer.readAnswer(questionId,function(err,result){
            if (err){
                res.status(400).send({ error:true, message: 'please contact support@u-self.com' });
            }
            res.json(result);
        })
    }


}
// export function read_modules_questions_by_courseId(req,res){
//     const courseId = req.params.courseId


//     Question.readModulesQuestionsByCourseId(courseId, function(err,result){
//         if (err){
//             res.send(err);
//         }

//         var modules = [];
                
//         var module = {};
//         var prevId = null;

//         var questions = [];


        
//         result.forEach((element,index) => {
            
//             if (element.id !== prevId){
//                 //to skip first case
//                 if(prevId != null){

//                     //push data into array
//                     module.questions = questions;
//                     modules.push(module);

//                     //reset module object and question
//                     module ={};
//                     questions = [];
//                 }
                
//                 //set module
//                 module.id = element.id;
//                 module.module_number = element.module_number;
//                 module.module_name = element.module_name;
//                 module.module_description = element.module_description;
//                 module.media = element.media;
//                 prevId = element.id;
                
//             }

//             if(element.q_id !== null){
//                 var question = {};

//                 question.question = element.question;
//                 question.media = element.media;
//                 question.id = element.q_id;
//                 question.answer_number = element.answer_number;
//                 question.mark = element.mark;
    
//                 questions.push(question);
//             }

            
//             //for last 
//             if (Object.is(result.length - 1, index)) {
             
//                 //push data into array
//                 module.questions = questions;
//                 modules.push(module);

//                 //reset module object and question
//                 module ={};
//                 questions = [];

//             }

        
//         });

//         res.send(modules)  
//     })
// }