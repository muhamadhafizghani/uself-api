"use strict";
import UserMark from '../models/userMarkModel';
import  config  from "config";
import Answer from '../models/answerModel';
import axios from 'axios';
import {isEmpty} from '../../../utils/index';


export function update_answer(req,res){

    var user_mark = new UserMark(req.body);
    user_mark.user_id = req.user.id;
    user_mark.question_id = req.params.questionId;

    if (!req.body.answer){

        res.status(400).send({ error:true, message: "Please provide user's answer" });

    }else{

        Answer.readAnswer( user_mark.question_id, async function(err,result){

            if(isEmpty(result[0].answer)){
                res.status(400).send({ error:true, message:'There is no answer provided please contact support@u-self.com' });
            }else{
                // const response = compareAnswer(user_mark.answer,result[0].answer)
                try {
    
                    const conf = {
                        headers: {
                            "x-rapidapi-host": config.get("similarityX-RapidAPI-Host"),
                            "x-rapidapi-key": config.get("similarityX-RapidAPI-Key"),        
                        },
                        params: {
                            'text1': user_mark.answer,
                            'text2': result[0].answer
                        }
            
                    };
            
                    
                    const response = await axios.get(config.get("similarityURL"),conf);
    
                    user_mark.similarity = response.data.similarity;
                    
                    //mark assigned
                    if(user_mark.similarity >= 0.6){
                        user_mark.mark = result[0].mark
                    }else{
                        user_mark.mark = 0
                    }
        
                    UserMark.updateAnswer(user_mark, function(err,result){
    
                        if (err){
                            res.status(400).send({ error:true, message:'please contact support@u-self.com' });
                        }else{
                            if(result.affectedRows == 0){
                                res.status(403).send({ error:true, message:'User are not allowed to change answer for the answered question' });

                            }else{
                                res.status(200).send({ error:false, message:'marks successfully updated', user_mark });

                            }
                        }
    
                    })
    
    
    
    
    
    
    
            
                } catch(e) { 
                    console.log(e);
                }//endtrycatch
            
            }//endif


        })
    }
}

export function read_user_mark(req, res){

    const user_mark = new UserMark({course_id: req.params.courseId, user_id: req.user.id})

    UserMark.readAnswer(user_mark, function(err,result){
        if (err)
            res.status(400).send({ error:true, message: err.sqlMessage });
        else
            res.json(result);
    });

}