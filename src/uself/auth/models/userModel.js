"use strict";
import  connection  from '../../../../config/db';
import fs from 'fs';
import path from 'path';

export class User {

    //User object constructor
    constructor(user) {
        this.id = user.id;
        this.first_name = user.first_name;
        this.last_name = user.last_name;
        this.phone = user.phone;
        this.email = user.email;
        this.avatar = user.avatar;
        this.password = user.password;
        this.date_of_birth = user.date_of_birth;
        this.address_number = user.address_number;
        this.address_street = user.address_street;
        this.address_city = user.address_city;
        this.address_postal_code = user.address_postal_code;
        this.address_state = user.address_state;
    }

    static writeUser(new_user, result) {
        try {
            connection.query("INSERT INTO user set ?", new_user, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }
    }

    static readUserPassword(user, result) {
        try {
            connection.query("Select id, password, role from user where email = ?", user.email, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }
    }

    static readUserById(userId, result) {
        try { 
            
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_user.sql')).toString();
            connection.query( sql+ " where id = ? ", userId, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }

    }

    static readAllUser(result){
        try {

            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_user.sql')).toString();
            connection.query(sql, function (err, res) {

                if (err) {
                    console.log(err);
                    result(null, err);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }

    }

    static updateUser(user,result){

        try {

            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/update_user.sql')).toString();
            let param = [   
                user.phone,
                user.avatar, 
                user.address_number, 
                user.address_street,
                user.address_city,
                user.address_postal_code ,
                user.address_state,
                user.id,
                user.id
            ]
            connection.query(sql, param, function (err, res) {

                if (err) {
                    console.log(err);
                    result(null, err);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }

    }


    static deleteUser(userId, result) {
        try {
            connection.query("DELETE FROM user WHERE id = ?", [userId], function (err, res) {

                if (err) {
                    console.log(err);
                    result(null, err);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }
    }

}

export default User;