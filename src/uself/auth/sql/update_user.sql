UPDATE user
SET
    phone = ?,
    avatar = ?, 
    address_number = ?, 
    address_street = ?,
    address_city = ?,
    address_postal_code = ?,
    address_state = ?
WHERE 
    id = ?