import User from '../models/userModel';
import { checkPassword } from '../../../utils/hashing';
import  jwt  from 'jsonwebtoken';
import  config  from 'config';



//login for user
export function user_login(req, res) {

    //check auth header exist
    var authHeader = req.headers.authorization;
    if(!authHeader){
        var err = new Error("Authorization header is not provided");
        err.status = 401;
        next(err);
        return;
    }

    //extract the email and password from auth header
    var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
    var email = auth[0];
    var password = auth[1];

    //set the email and password from auth header
    var user = new User({email:email,password:password});

    //check email whether the email is provided
    if(!email){

        res.status(400).send({ error:true, message: 'Please provide email' });

    //check whether the password is provided
    }else if(!password){

        res.status(400).send({ error:true, message: 'Please provide password' });

    }else{

        //retrieve password from database
        User.readUserPassword(user, function(err, result) {
  
            //sql error
            if (err)

                res.status(400).send({ error:true, message: "Please contact support@u-self.com"});

            else{
                
                //the email doesnt exist in the database
                if(result[0] == null){

                    res.status(401).send({ error:true, message: "User does not exist"});

                }else{
                //password hashing validation

                    var passwordHash = checkPassword(user.password,result[0].password.slice(128,144)).passwordHash;
        
                    if (result[0].password.slice(0,128) == passwordHash){

                        //return token
                        const payload = {
                            user: {
                              id: result[0].id,
                              role: result[0].role
                            }
                        };


                        jwt.sign(
                            payload,
                            config.get("jwtSecret"),
                            { expiresIn: '3d' },
                            (err, token) => {
                                if (err) throw err;
                                res.json({ token, id: result[0].id });
                            }
                            );
                        
                    

                    } else{

                        res.status(401).send({ error:true, message: "Password or email is invalid"});
        
                    } 
                }

            }


        });

    }


}

