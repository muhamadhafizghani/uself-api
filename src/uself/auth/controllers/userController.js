"use strict";
import User from '../models/userModel';
// import {  readAllUser, readUserById, deleteUser } from '../models/userModel';
import { hashPassword } from '../../../utils/hashing';


//register or sign up a user
export function write_a_user(req, res) {
  var new_user = new User(req.body);

   //handles null error 
   if(!new_user.first_name){

        res.status(400).send({ error:true, message: 'Please provide first name' });

    }else if(!new_user.last_name){

        res.status(400).send({ error:true, message: 'Please provide last name' });

    }else if(!new_user.email){

        res.status(400).send({ error:true, message: 'Please provide email' });

    }else if(!new_user.password){

        res.status(400).send({ error:true, message: 'Please provide password' });

    }else if(!new_user.phone){

        res.status(400).send({ error:true, message: 'Please provide phone number' });

    }else if(!new_user.date_of_birth){

        res.status(400).send({ error:true, message: 'Please provide date of birth' });

    }else{
  
    
      //hash the password
      new_user.password = hashPassword(new_user.password).passwordHash;

      //write user into database
      User.writeUser(new_user, function(err, result) {
            
            if (err){

              if(err.sqlMessage.toLowerCase().includes("duplicate entry") && err.sqlMessage.toLowerCase().includes("email") ){
                res.status(400).send({ error:true, message: "Email is already being used"});
              }else{
                res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
              }

            }else{

              res.json({ error:false, message: "User is successfully registered"});

            }

        });
    }
}

//get all users from the database
export function read_all_users(req, res) {

  User.readAllUser(function(err, user) {

    if (err)
      res.status(400).send({ error:true, message: "Please contact support@u-self.com"});

    res.send(user);

  });

}


//get user by id
export function read_a_user(req, res) {
  User.readUserById(req.user.id, function(err, user) {
    
    if (err)
        res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
    else
        res.json(user);
  });
}


export function update_user(req,res){
  var update_user = new User(req.body);
  update_user.id = req.user.id;

  User.updateUser(update_user, function(err,user){
    if (err)
      res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
    else
      res.json({ message: 'User successfully updated' });
  })

}


//delete user from database
export function delete_a_user(req, res) {


  User.deleteUser( req.user.id, function(err, user) {
    if (err)
      res.send(err);
    res.json({ message: 'User successfully deleted' });
  });
}