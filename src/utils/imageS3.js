import AWS from "aws-sdk";
//import sharp from "sharp";
import config from "config";

const BUCKET_NAME = config.get("BUCKET_NAME")
const IAM_USER_KEY = config.get("IAM_USER_KEY");
const IAM_USER_SECRET = config.get("IAM_USER_SECRET");

const S3Bucket = new AWS.S3({
  accessKeyId: IAM_USER_KEY,
  secretAccessKey: IAM_USER_SECRET,
  Bucket: BUCKET_NAME
});

export function upload(filename, folder, file) {
  return new Promise((resolve, reject) => {
    S3Bucket.createBucket(function() {
      const params = {
        ACL: "public-read",
        Bucket: BUCKET_NAME,
        Key: folder + filename,
        Body: file.data,
        ContentType: "image/png"
      };

      S3Bucket.upload(params, function(err, data) {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  });
};


