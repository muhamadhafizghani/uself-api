import config from "config"
import axios from "axios";
import {AUTHENTICATION} from "../../../config/config"

export async function badgrToken(){
    try {


        const params = { 
          
          password: config.get("passwordBadgr"),
          username: config.get("emailBadgr")

         
        };

        const res = await axios.post(AUTHENTICATION, null, {params});
        // console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        // console.log(res)
       return {
        token:res.data.access_token
       }

    } catch (err) {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        console.log(err)
        return err.response.data
    }
}



