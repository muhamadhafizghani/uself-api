import axios from "axios";
import {DIGIBADGE_AUTHENTICATION} from "../../../config/config";
import config from "config";

export async function digiBadgeAuth(){
    
      try{
        var basicAuth = 'Basic ' + Buffer.from(`${config.get("DIGIBADGEUSER")}:${config.get("DIGIBADGEPASSWORD")}`).toString('base64');
        const conf= {
            headers: {
        
              "Content-Type": "application/json",
              "Authorization": basicAuth
        
            }
        };

        const res = await axios.post(DIGIBADGE_AUTHENTICATION, {}, conf);
        // console.log("***********************************************************")
        // console.log(res.data.token)

       return {
        token:res.data.token
       }

      }catch(err){
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        console.log(err)
        return err.response.data
      }


    
}
