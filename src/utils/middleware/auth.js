import jwt from "jsonwebtoken";
import  config  from "config";

export default function(req, res, next) {
  //Get token from header
  const header = req.headers['authorization'];

  if(typeof header !== 'undefined') {
      const bearer = header.split(' ');
      const token = bearer[1];

      //Verify token
      try {
        const decoded = jwt.verify(token, config.get("jwtSecret"));
        req.user = decoded.user;
        next();
      } catch (err) {
        console.log(err)
        res.status(403).json({ error:true, message: "Token is not valid" });
      }  

  } else {
      //If header is undefined return Forbidden (403)
      return res.status(403).json({ error:true, message: "No token, authorization denied" });
  }
};
