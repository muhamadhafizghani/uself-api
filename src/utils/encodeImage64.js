import fs from 'fs';


// function to encode file data to base64 encoded string
export function base64_encode(data) {
    // read binary data
    var data = "data:image/png;base64," + new Buffer(data).toString('base64');

    // convert binary data to base64 encoded string
    return data;
  }