'use strict';
import { randomBytes, createHmac } from 'crypto';

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
var genRandomString = function(length){
    return randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};

// /**
//  * hash password with sha512.
//  * @function
//  * @param {string} password - List of required fields.
//  * @param {string} salt - Data to be validated.
//  */
// exports.sha512 = function(password, salt){
//     var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
//     hash.update(password);
//     var value = hash.digest('hex');
//     return {
//         salt:salt,
//         passwordHash:value
//     };
// };

/*
    Hashpassword+salt
 */
export function hashPassword(userpassword) {

    var salt = genRandomString(16); /** Gives us salt of length 16 */
    var hash = createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(userpassword);
    var value = hash.digest('hex');
    //  console.log('UserPassword = '+ userpassword);
    // console.log('Passwordhash = '+ value);
    // console.log('nSalt = '+ salt);
    return {
        passwordHash: value+salt,
    };
}

export function checkPassword(userpassword, salt){

    var hash = createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(userpassword);
    var value = hash.digest('hex');
    return {
        passwordHash: value,
    };

}

//console.log(this.hashPassword('MYPASSWORD').passwordHash)
// saltHashPassword('MYPASSWORD');
// saltHashPassword('MYPASSWORD');