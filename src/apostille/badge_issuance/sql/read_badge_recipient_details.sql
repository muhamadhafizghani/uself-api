SELECT 
    R.hash,
    R.badge_image_apostille,
    R.recipient_name,
    R.recipient_email,
    R.description,
    R.time_created,
    B.badge_name,
    B.badge_description,
    U.company_name,
    R.qr_code_url,
    R.badge_image_url,
    R.url_evidence

FROM 
    badge_recipient R
LEFT JOIN apostille_badge B ON B.badgeclass_id = R.badgeclass_id
LEFT JOIN apostille_user U ON U.issuer_id = B.issuer_id 
WHERE assertion_id = ?