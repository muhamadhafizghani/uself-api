import connection from '../../../../config/db';
import {removeNull} from '../../../utils/removeNull';
import fs from 'fs';
import path from 'path';

export class BadgeRecipient{

    constructor(badge_recipient){
        this.assertion_id = badge_recipient.assertion_id;
        this.badgeclass_id =badge_recipient.badgeclass_id;
        this.hash = badge_recipient.hash;
        this.recipient_email = badge_recipient.recipient_email;
        this.recipient_name = badge_recipient.recipient_name;
        this.badge_image_url = badge_recipient.badge_image_url;
        this.qr_code_url = badge_recipient.qr_code_url;
        this.badge_image_apostille = badge_recipient.badge_image_apostille;
        this.time_created = badge_recipient.time_created;
        this.url_evidence = badge_recipient.url_evidence;
        this.description = badge_recipient.description;
        this.time_updated = badge_recipient.time_updated;
    }

    static writeBadgeRecipient(badge_recipient, result){

        try {

            let sql = 'SELECT write_badge_recipient(?,?,?,?,?,?,?,?,?,?,?) AS id'
            let param = [   badge_recipient.assertion_id,
                            badge_recipient.badgeclass_id,
                            badge_recipient.hash,
                            badge_recipient.badge_image_url,
                            badge_recipient.badge_image_apostille,
                            badge_recipient.recipient_name,
                            badge_recipient.recipient_email,
                            badge_recipient.description,
                            badge_recipient.url_evidence,
                            badge_recipient.qr_code_url,
                            badge_recipient.time_created
                        ]

            
            connection.query(sql, param, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }

    }

    static readBadgeRecipientById(id,result){
        try {
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/read_badge_recipient_details.sql')).toString();
            connection.query(sql, id, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }
    }

    static readBadgeRecipientsByBadgeId(id,result){
        try{
            connection.query("SELECT * FROM badge_recipient WHERE badgeclass_id = ?", id, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }catch(err){
            console.log(err);
            result(err, null);
        }
    }
}

export default BadgeRecipient;