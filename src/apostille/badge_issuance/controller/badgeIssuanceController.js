import config from 'config';
import BadgeRecipient from '../models/badgeRecipient';
import moment from 'moment-timezone';
import {badgrToken} from '../../../utils/middleware/badgrAuth';
import {POST_ASSERTION, DELETE_ASSERTION} from '../../../../config/config';
import nem from 'nem-sdk';
import axios from "axios";
import {isValidURL} from "../../../utils/isValidURL"
import {upload} from "../../../utils/imageS3";
import QRCode from 'qrcode';

export function write_apostille_ebadge_issuance(req,res){

  var new_recipient = new BadgeRecipient(req.body);

  //error check
  if(!new_recipient.recipient_email){

    res.status(400).send({ error:true, message: 'Please provide email address of recipient' });

  }else if(!new_recipient.recipient_name){

    res.status(400).send({ error:true, message: 'Please provide recipient name' });

  }else if(!new_recipient.description){

    res.status(400).send({ error:true, message: 'Please provide narrative description' });

  }else if(!new_recipient.url_evidence ){

    res.status(400).send({ error:true, message: 'Please provide url' });

  }else if(!isValidURL(new_recipient.url_evidence)){

    res.status(400).send({ error:true, message: 'Please provide valid url ex: http://apostille.u-self.com' });

  }else if(!new_recipient.badgeclass_id){

    res.status(400).send({ error:true, message: 'Please provide badge id' });

  }else{

    //token
    var auth_badgr_token = null;
    new_recipient.time_created = moment().format('YYYY-MM-DDTHH:mm:ssZ');

    //get token
    badgrToken().then((response)=>{

      //post assertion
      try {

        //set token
        auth_badgr_token = response.token
        const header = {
          headers: {
            "Content-Type": "application/json",
            'Authorization': "bearer " + auth_badgr_token
          }
        };

        //set request body
        const body = JSON.stringify({
            "recipient": {
                "identity": new_recipient.recipient_email,
                "hashed": true,
                "type": "email",
                "plaintextIdentity": new_recipient.recipient_email
                },
            "issuedOn": new_recipient.time_created,
            "narrative": `Recipient: ${new_recipient.recipient_name} [${new_recipient.description}]`,
            "evidence": [
                {
                    "url": new_recipient.url_evidence           }
            ]
            
        });
         
        //send request
        return axios.post(POST_ASSERTION(new_recipient.badgeclass_id), body, header);
                
      } catch (err) {

        console.log("response failed")
        console.log(err)
        res.status(400).send({error:true, message:"Please contact support@u-self.com"})

      }
            
            
            
    }, (err) =>{

      console.log(err)
      res.status(400).send({error:true, message:"Please contact support@u-self.com"})

    }).then((response) => {
            
      // apostille badge recipient

      // Custom endpoint NEM
      var endpoint = nem.model.objects.create("endpoint")("http://hugetestalice.nem.ninja", 7890);

      //set data to write in database
      new_recipient.assertion_id = response.data.result[0].entityId;
      new_recipient.badge_image_url = response.data.result[0].image;

      // Create a common object holding key
      var common = nem.model.objects.create("common")(config.get("nemWalletPassword"), config.get("nemPrivateKey"));

      //digital badge image request
      var request = require('request').defaults({ encoding: null });

      request.get(response.data.result[0].image, function (error, respo, body) {

        //success request
        if (!error && respo.statusCode == 200) {

          //file to base 64
          var data = "data:" + respo.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');

          // Remove the meta part of data string (data:application/octet-stream;base64)
          var cleanedDataContent = data.split(/,(.+)?/)[1];
          
          // Base 64 to word array
          var parsedData = nem.crypto.js.enc.Base64.parse(cleanedDataContent);

          // filename
          var fileName = `assertion-${response.data.result[0].entityId}.png`
          
          // Create the Apostille
          var apostille = nem.model.apostille.create(common, fileName, parsedData, response.data.result[0].recipient, nem.model.apostille.hashing["SHA256"], false, {}, true, nem.model.network.data.testnet.id);
          setTimeout(sendtransaction,3000);
                    

          // Serialize transfer transaction and announce
          function sendtransaction() {

            nem.model.transactions.send(common, apostille.transaction, endpoint).then( result => {

              if (result.code >= 2) {

                console.error(result);
                res.status(400).send({error:true, message:"Please contact support@u-self.com"});

              } else {

                //apostille time stamp
                var timeStamp = moment().format('DD-MM-YYYY')

                //badge apostille file name
                var var_name_of_file = (apostille.data.file.name.replace(/.[^/.]+$/, "") + " -- Apostille TX " + result.transactionHash.data +  " -- Date " + timeStamp + "." + apostille.data.file.name.split('.').pop()).toString();

                //create the data for image 
                var imageBuffer = decodeBase64Image(data);


                //upload image to S3
                const compImg = { data: imageBuffer.data, mimetype: data.mimetype };
                const S3Folder = "ebadge/";
                const done = upload(var_name_of_file, S3Folder, compImg);
                
                //error
                if (!done) {
                  return res
                    .status(409)
                    .json({ msg: "There is an error uploading the Apostille image" });
                }


                //create image and save to local dircetory

                // fs.writeFile(`D:/Development/fyp/image-test/${var_name_of_file}`, imageBuffer.data, function (err) {
                //     if (err){
                //         console.log(err); 

                //         throw err;
                //     }
                //     console.log('The file has been saved!');
                //     });

                //set data to write in database
                new_recipient.hash = result.transactionHash.data
                new_recipient.badge_image_apostille = `https://apostille-badge.s3-ap-southeast-1.amazonaws.com/ebadge/${var_name_of_file}`

                //write into database
                BadgeRecipient.writeBadgeRecipient(new_recipient, function(err,result){
                  
                  if (err){
                    
                    //delete badge that has being created
                    try {
                      const config = {
                        headers: {
                          "Content-Type": "application/json",
                          'Authorization': "bearer " + auth_badgr_token
                        }
                      };
              
                      axios.delete(DELETE_ASSERTION + `/${new_recipient.assertion_id}`,config);
              
                    } catch (err) {
                        console.log(err)
                    }

                    
                    //query error
                    if(err.sqlMessage.toLowerCase().includes("duplicate entry")){

                      res.status(400).send({ error:true, message: "Assertion Id is already being used"});

                    }else{

                      console.log(err)
                      res.status(400).send({ error:true, message: "Please contact support@u-self.com"});

                    }
              

                  }else{

                    //assign id if change ()
                    new_recipient.assertion_id = result[0].id;

                  }//end if

                  res.status(200).send({
                    transactionHash: new_recipient.hash,
                    apostille_path: new_recipient.badge_image_apostille,
                    assertion_id: new_recipient.assertion_id,
                    badgeclass_id: new_recipient.badgeclass_id,
                    qr_code: new_recipient.qr_code_url,
                    time_created: new_recipient.time_created
                  })

              
                })

              }//end if
            
            })
          
          }// function send transaction



        }else{

          console.log("The image is not retrieved and apostille did not send")
          res.status(400).send({error:true, message:"Please contact support@u-self.com"})

        }//end if

      });

    }, (err) =>{
      console.log(err)
      res.status(400).send({error:true, message:"Please contact support@u-self.com"})
    }).then( () =>{

      var opts = {
        errorCorrectionLevel: 'H',
        type: 'image/jpeg',
        quality: 0.3,
        margin: 1,
        color: {
          dark:"#000000",
          light:"#ffffff"
        }
      }

      QRCode.toDataURL(`https://u-self.com/apostille/validation/${new_recipient.assertion_id}`, opts, function (err, url) {
        if (err){
          console.log(err)
          res
            .status(400)
            .json({ msg: "There is error to generate qr" });
        }
       
        
        // console.log(url)


        //create the data for image 
        var imageBuffer = decodeBase64Image(url);
        var file_name = `qr-${new_recipient.assertion_id}.png`;


        //upload image to S3
        const compImg = { data: imageBuffer.data, mimetype: url.mimetype };
        const S3Folder = "qr/";
        const done = upload(file_name, S3Folder, compImg);
        
        //error
        if (!done) {
          return res
            .status(409)
            .json({ msg: "There is an error uploading the QR image" });
        }

        //set qr_code to save in database
        new_recipient.qr_code_url = `https://apostille-badge.s3-ap-southeast-1.amazonaws.com/qr/${file_name}`

      })

    })

  }//end else

}

//function to create image base on base64
function decodeBase64Image(dataString) {
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
    response = {};

  if (matches.length !== 3) {
    return new Error('Invalid input string');
  }

  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');

  return response;
}//function decodeBase64Image