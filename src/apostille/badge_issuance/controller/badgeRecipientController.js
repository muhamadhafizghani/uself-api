import BadgeRecipient from '../models/badgeRecipient';


export function read_apostille_ebadge_recipient_by_badge_id(req,res){

    var badge_id = req.body.badge_id;

    if(!badge_id){
        res.status(400).send({ error:true, message: 'Please provide badge id' });
    }

    BadgeRecipient.readBadgeRecipientsByBadgeId(badge_id, function(err, result) {
        if (err){
        console.log(err)
            res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
        }else{
            res.json(result);
        }
    })

}

export function read_apostille_ebadge_recipients(req,res){

    var assertionId = req.params.assertionId

    BadgeRecipient.readBadgeRecipientById(assertionId, function(err, result) {
        if (err){
            console.log(err)
            res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
        }else{
            res.json(result);
        }
    }) 

}