import axios from 'axios';
import ApostilleBadge from '../models/apostilleBadge';
import {badgrToken} from '../../../utils/middleware/badgrAuth';
import {BADGECLASSES} from '../../../../config/config';
import {base64_encode} from '../../../utils/encodeImage64';

export function write_apostille_badge(req,res){

  // console.log(req.body)
  var body = JSON.parse(req.body.data)
  var new_badge = new ApostilleBadge(body);


  //set param for write to database
  new_badge.issuer_id = req.user.issuer_id;
  if (!req.files) {
    return res.status(400).json({ msg: "No image provided" });
  }
  //error checking
  if(!new_badge.badge_description){

    res.status(400).send({ error:true, message: 'Please provide badge description' });

  }else if(!new_badge.badge_name){

    res.status(400).send({ error:true, message: 'Please provide badge name' });

  }else if(req.files.image.mimetype !=="image/png"){
    res.status(400).send({ error:true, message: 'Please provide badge image png format' });

  }else if(!body.criteria){

    res.status(400).send({ error:true, message: 'Please provide criteria narrative' });

  }else{

    var auth_badgr_token = null;

    //get token
    badgrToken().then((response)=>{
      
      //post badge class open badgr
      try {

        auth_badgr_token = response.token
        const header = {
          headers: {
            "Content-Type": "application/json",
            'Authorization': "bearer " + auth_badgr_token
          }
        };

        const body = JSON.stringify({ 
          
          "issuer": req.user.issuer_id,
          "name": new_badge.badge_name,
          "description": new_badge.badge_description,
          "image": base64_encode(req.files.image.data),
          "criteriaNarrative" : req.body.data.criteria

          
        });

        return axios.post(BADGECLASSES, body, header);
        


      } catch (err) {
        console.log(err)
      }
        
        
        
    }, (err) =>{
  
      console.log(err)
      res.status(400).send({error:true, message:"Please contact support@u-self.com"})
  
    }).then((response)=>{
  
      //set param for write to database
      new_badge.badgeclass_id = response.data.result[0].entityId;
      new_badge.badge_url = response.data.result[0].openBadgeId;
      new_badge.image = response.data.result[0].image;

      //write user into database
      ApostilleBadge.writeApostilleBadge(new_badge, function(err, result) {
            
        if (err){
          
          //delete badge that has being created if err
          try {
              const config = {
                headers: {
                  "Content-Type": "application/json",
                  'Authorization': "bearer " + auth_badgr_token
                }
              };
      
              axios.delete(BADGECLASSES + `/${new_badge.badgeclass_id}`,config);
              
      
      
            } catch (err) {
                console.log(err)
            }

          //send err message 
          if(err.sqlMessage.toLowerCase().includes("duplicate entry")){

            res.status(400).send({ error:true, message: "Badge Id is already being used"});

          }else{

            console.log(err)
            res.status(400).send({ error:true, message: "Please contact support@u-self.com"});

          }

        }else{
          //success response result -badgeclass id
          res.json(result);
        }

      });
        
        
    }, (err) =>{

      console.log(err)
      res.status(400).send({error:true, message:"Please contact support@u-self.com"})
  
    });


  }//end if





}

export function read_apostille_badges(req,res){

  ApostilleBadge.readApostilleBadges(req.user.issuer_id, function(err, result) {
      
    if (err){
      console.log(err)
      res.status(400).send({ error:true, message: "Please contact support@u-self.com"});
    }else{
      res.json(result);
    }

  });

}


export function read_apostille_badge_by_id(req,res){
  const badgeId = req.params.badgeId

  ApostilleBadge.readApostilleBadgeById(badgeId,req.user.issuer_id, function(err, result) {
      
    if (err){
      console.log(err)
      res.status(400).send({ error:true, message: "Please contact support@u-self.com"});

    }else{
      res.json(result);
    }

  });

}
 

