import connection from '../../../../config/db';
import {removeNull} from '../../../utils/removeNull';

export class ApostilleBadge{

    constructor(apostille_badge){
        this.badgeclass_id = apostille_badge.badgeclass_id;
        this.issuer_id = apostille_badge.issuer_id;
        this.badge_name = apostille_badge.badge_name;
        this.image = apostille_badge.image;
        this.badge_description = apostille_badge.badge_description;
        this.badge_url = apostille_badge.badge_url;
        this.time_created = apostille_badge.time_created;
        this.time_updated = apostille_badge.time_updated;
    }

    static writeApostilleBadge(apostille_badge, result){

        //removeNull(apostille_badge);

        try {

            let sql = 'SELECT write_apostille_badge(?,?,?,?,?,?) AS id'
            let param = [   apostille_badge.badgeclass_id,
                            apostille_badge.issuer_id,
                            apostille_badge.badge_name,
                            apostille_badge.badge_description,
                            apostille_badge.image,
                            apostille_badge.badge_url
                        ]
            connection.query(sql, param, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }

    }

    static readApostilleBadgeById(badgeId,issuerId,result){

        let param = [   
            badgeId,
            issuerId
        ]

        try {
            connection.query("SELECT * FROM apostille_badge WHERE badgeclass_id = ? AND issuer_id = ?", param, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }
    }

    static readApostilleBadges(issuerId,result){
        try {
            connection.query("SELECT * FROM apostille_badge WHERE issuer_id = ?", issuerId, function (err, res) {

                if (err) {
                    console.log(err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }
    }


}
export default ApostilleBadge;