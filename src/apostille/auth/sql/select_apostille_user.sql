Select 
    issuer_id, 
    company_name, 
    company_registration_number, 
    image,
    description,
    role,
    url, 
    phone, 
    email, 
    date_start_operate, 
    address_number, 
    address_street, 
    address_city, 
    address_postal_code, 
    address_state,
    time_created,
    time_updated 
from 
    apostille_user