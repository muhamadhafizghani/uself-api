"use strict";
import  connection  from '../../../../config/db';
import fs from 'fs';
import path from 'path';
import {removeNull} from '../../../utils/removeNull'

export class ApostilleUser {

    //User object constructor
    constructor(apostille_user) {
        this.issuer_id = apostille_user.issuer_id;
        this.company_name = apostille_user.company_name;
        this.company_registration_number = apostille_user.company_registration_number;
        this.image = apostille_user.image;
        this.description = apostille_user.description;
        this.phone = apostille_user.phone;
        this.url = apostille_user.url;
        this.email = apostille_user.email;
        this.role = apostille_user.role;
        this.password = apostille_user.password;
        this.date_start_operate = apostille_user.date_start_operate;
        this.address_number = apostille_user.address_number;
        this.address_street = apostille_user.address_street;
        this.address_city = apostille_user.address_city;
        this.address_postal_code = apostille_user.address_postal_code;
        this.address_state = apostille_user.address_state;
    }

    static writeApostilleUser(new_user, result) {

        removeNull(new_user)
        try {
            connection.query("INSERT INTO apostille_user set ?", new_user, function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else {
                    console.log(res);
                    result(null, res);
                }
            });
        }
        catch (err) {
            console.log(err);
            result(err, null);
        }
    }

    static readApostilleUserPassword(user, result) {
        try {
            connection.query("Select issuer_id, password, role from apostille_user where email = ?", user.email, function (err, res) {

                if (err) {
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }
    }

    static readApostilleUserById(userId, result) {
        try { 
            
            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_apostille_user.sql')).toString();
            connection.query( sql+ " where issuer_id = ? ", userId, function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }

    }

    static readAllApostilleUser(result) {
        try {

            var sql = fs.readFileSync(path.resolve(__dirname,'../sql/select_apostille_user.sql')).toString();
            connection.query(sql, function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else {
                    //console.log('user : ', res);
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }

    }


    static deleteApostilleUser(userId, result) {
        try {
            connection.query("DELETE FROM apostille_user WHERE issuer_id = ?", [userId], function (err, res) {

                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else {
                    result(null, res);
                }
            });
        }
        catch (err) {

            console.log(err);
            result(err, null);
        }
    }

}

export default ApostilleUser;