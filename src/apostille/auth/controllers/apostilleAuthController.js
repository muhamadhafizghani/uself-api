import ApostilleUser from '../models/apostilleUserModel';
import { checkPassword } from '../../../utils/hashing';
import  jwt  from 'jsonwebtoken';
import  config  from 'config';



//login for user
export function apostille_user_login(req, res) {

    //check auth header exist
    var authHeader = req.headers.authorization;
    if(!authHeader){
        var err = new Error("authorization header is not provided");
        err.status = 401;
        next(err);
        return;
    }

    //extract the email and password from auth header
    var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
    var email = auth[0];
    var password = auth[1];

    //set the email and password from auth header
    var user = new ApostilleUser({email:email,password:password});

    //check email whether the email is provided
    if(!email){

        res.status(400).send({ error:true, message: 'Please provide email' });

    //check whether the password is provided
    }else if(!password){

        res.status(400).send({ error:true, message: 'Please provide password' });

    }else{

        //retrieve password from database
        ApostilleUser.readApostilleUserPassword(user, function(err, result) {
  
            //sql error
            if (err)

                res.status(400).send({ error:true, message: err.sqlMessage });

            else{
                
                //the email doesnt exist in the database
                if(result[0] == null){

                    res.status(401).send({ error:true, message: "User does not exist"});

                }else{
                //password hashing validation

                    var passwordHash = checkPassword(user.password,result[0].password.slice(128,144)).passwordHash;
        
                    if (result[0].password.slice(0,128) == passwordHash){

                        //return token
                        const payload = {
                            user: {
                              issuer_id: result[0].issuer_id
                            }
                        };

                        jwt.sign(
                            payload,
                            config.get("jwtSecretApostille"),
                            { expiresIn: 3600 },
                            (err, token) => {
                                if (err) throw err;
                                res.json({ token });
                            }
                            );
                        
                    

                    } else{

                        res.status(401).send({ error:true, message: "Unauthorized"});
        
                    } 
                }

            }


        });

    }


}

