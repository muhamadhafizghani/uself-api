"use strict";
import ApostilleUser from '../models/apostilleUserModel';
// import {  readAllUser, readUserById, deleteUser } from '../models/userModel';
import  config  from "config";
import {badgrToken} from "../../../utils/middleware/badgrAuth";
import { hashPassword } from '../../../utils/hashing';
import {ISSUER} from "../../../../config/config"
import axios from "axios";
import {base64_encode} from '../../../utils/encodeImage64';
import {isValidURL} from '../../../utils/isValidURL';



//register or sign up a user
export function write_an_apostille_user(req, res) {
  
  var body = JSON.parse(req.body.data)
  var new_user = new ApostilleUser(body);

  // console.log(req.body)
  // console.log(new_user)
  //handles null error 
  if(!new_user.company_name){

      res.status(400).send({ error:true, message: 'Please provide company name' });

  }else if(!req.files) {

      res.status(400).send({ error:true, message: "Company logo png format is not provided" });

  }else if(req.files.image.mimetype !=="image/png"){

    res.status(400).send({ error:true, message: 'Please provide logo image in png format' });

  }else if(!new_user.company_registration_number){

      res.status(400).send({ error:true, message: 'Please provide company registration number' });

  }else if(!new_user.email){

      res.status(400).send({ error:true, message: 'Please provide email' });

  }else if(!new_user.password){

      res.status(400).send({ error:true, message: 'Please provide password' });

  }else if(!new_user.phone){

      res.status(400).send({ error:true, message: 'Please provide phone number' });

  }else if(!new_user.date_start_operate){

      res.status(400).send({ error:true, message: 'Please provide the date company start to operate' });

  }else if(!new_user.url || !isValidURL(new_user.url)){

    res.status(400).send({ error:true, message: 'Please provide the url of the company. Example: http://u-self.com' });

  }else{

  
    var auth_badgr_token = null;
    //hash the password
    new_user.password = hashPassword(new_user.password).passwordHash;


    //create issuer
    badgrToken().then((response)=>{

        try {
          auth_badgr_token = response.token
          const header = {
            headers: {
              "Content-Type": "application/json",
              'Authorization': "bearer " + auth_badgr_token
            }
          };
  
          const body = JSON.stringify({ 
            
            "name": new_user.company_name,
            "image": base64_encode(req.files.image.data),
            "email": config.get("emailBadgr"),
            "description": new_user.description,
            "url": new_user.url,
            "staff": [{
                        "user": config.get("userBadgrID"),
                        "role": "staff"
                    }]
  
            
          });
  
          return axios.post(ISSUER, body, header);
          
  
  
        } catch (err) {
            console.log(err)
        }
      
      
      
    }, (err) =>{

      console.log(err)
      res.status(400).send({error:true, message:"Please contact support@u-self.com"})

    }).then((response)=>{
      // console.log("++++++++++++++++++++++RSPONSE2++++++++++++++++++++++++++++++++++++++++")
      // console.log(response.data.result[0])

      new_user.issuer_id = response.data.result[0].entityId
      new_user.image = response.data.result[0].image

      //write user into database
      ApostilleUser.writeApostilleUser(new_user, function(err, result) {
            
        if (err){

          //delete issuer that has being created
          try {
            const config = {
              headers: {
                "Content-Type": "application/json",
                'Authorization': "bearer " + auth_badgr_token
              }
            };
    
            axios.delete(ISSUER + `/${new_user.issuer_id}`,config);
            
    
    
          } catch (err) {
              console.log(err)
          }

          if(err.sqlMessage.toLowerCase().includes("duplicate entry")){

            res.status(400).send({ error:true, message: "Company name or email or company reqistration number is already being used"});

          }else{

            res.status(400).send({ error:true, message: "Please contact support@u-self.com"});

          }

        }else{
          res.json({id: new_user.issuer_id});
        }

      });
      
      
    }, (err) =>{

      console.log(err)
      res.status(400).send({error:true, message:"Please contact support@u-self.com"})

    });
  
  }
}

//get all users from the database
export function read_all_apostille_users(req, res) {

  ApostilleUser.readAllApostilleUser(function(err, user) {

    console.log('controller')

    if (err)

      res.send(err);
      console.log('res', user);

    res.send(user);

  });

}


//get user by id
export function read_an_apostille_user(req, res) {
  ApostilleUser.readApostilleUserById(req.user.issuer_id, function(err, user) {
    
    if (err)
        res.send(err);
    else
        res.json(user);
  });
}


// exports.update_a_task = function(req, res) {
//   Task.updateById(req.params.taskId, new Task(req.body), function(err, task) {
//     if (err)
//       res.send(err);
//     res.json(task);
//   });
// };


//delete user from database
export function delete_an_apostille_user(req, res) {


  ApostilleUser.deleteApostilleUser( req.user.issuer_id, function(err, user) {
    if (err)
      res.send(err);
    res.json({ message: 'User successfully deleted' });
  });
}