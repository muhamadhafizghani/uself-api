# Setup database on AWS

1.  Create database in RDS
2.  Choose mysql engine, and under 'CONNECTIVITY' and 'ADDITIONAL CONNECTIVITY CONFIGURATION',
    set publicly accessible yes.
3.  After creating database, click link under VPC security groups
4.  Set Inbound and Outbound tab to TCP and My IP
5.  Left side panel, click PARAMETER GROUPS
6.  Create a new Parameter Group. On the dialog, select the MySQL family compatible to your MySQL database version, give it a name and confirm. 
7.  Select the just created Parameter Group and issue “Edit Parameters”.
8.  Look for the parameter ‘log_bin_trust_function_creators’ and set its value to ’1′.
9.  Save the changes
10. modify the db and pick the group parameters that just created
11. save changes