SELECT  U.badge_level

FROM
    enrollment E
LEFT JOIN course C ON C.id = E.course_id
LEFT JOIN badge B ON B.course_id = C.id
LEFT JOIN badge_user U ON B.course_id = E.course_id AND U.badge_id = B.id
WHERE 
    E.user_id = 1 AND E.course_id = 1
