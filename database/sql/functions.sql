DROP FUNCTION IF EXISTS  `write_badge_recipient`;


delimiter //

CREATE FUNCTION write_badge_recipient ( 
    id VARCHAR(32),   
    badgeclass_id VARCHAR(32), 
    hash VARCHAR(100),
    badge_image_url VARCHAR(1000),
    badge_image_apostille VARCHAR(1000),
    recipient_name VARCHAR(100),
    recipient_email VARCHAR(255),
    description VARCHAR(500),
    url_evidence VARCHAR(1000),
    qr_code_url VARCHAR(1000),
    time_created TIMESTAMP )

RETURNS VARCHAR(32)

BEGIN

    DECLARE newId VARCHAR(32);
  IF id IS NULL THEN
    SET id = replace(uuid(),'-','');
  END IF;
    SET newId =id;

    INSERT INTO `badge_recipient`(  `assertion_id`,
                                    `badgeclass_id`,
                                    `hash`,
                                    `badge_image_url`,
                                    `badge_image_apostille`,
                                    `recipient_name` ,
                                    `recipient_email` ,
                                    `description` ,
                                    `url_evidence`,
                                    `qr_code_url`,
                                    `time_created` )
    VALUES (
        id,
        badgeclass_id,
        hash,
        badge_image_url,
        badge_image_apostille,
        recipient_name,
        recipient_email,
        description,
        url_evidence,
        qr_code_url,
        time_created  
    );

    RETURN newId;
END; //

delimiter ;

DROP FUNCTION IF EXISTS  `write_apostille_badge`;


delimiter //

CREATE FUNCTION write_apostille_badge ( 
    id VARCHAR(32),    
    issuer_id VARCHAR(32),
    badge_name VARCHAR(50),
    badge_description VARCHAR(500),
    image VARCHAR(1000),
    badge_url VARCHAR(1000))

RETURNS VARCHAR(32)

BEGIN

    DECLARE newId VARCHAR(32);
  IF id IS NULL THEN
    SET id = replace(uuid(),'-','');
  END IF;
    SET newId =id;

    INSERT INTO `apostille_badge`(  `badgeclass_id`,
                                    `issuer_id`,
                                    `badge_name`,
                                    `badge_description`,
                                    `image`,
                                    `badge_url` )
    VALUES (
        id,    
        issuer_id,
        badge_name,
        badge_description,
        image,
        badge_url 
    );

    RETURN newId;
END; //

delimiter ;