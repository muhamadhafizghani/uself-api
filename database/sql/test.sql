DROP TABLE IF EXISTS  `answer`;
DROP TABLE IF EXISTS  `user_mark`;
DROP TABLE IF EXISTS  `course_requirement`;
DROP TABLE IF EXISTS  `requirement`;
DROP TABLE IF EXISTS  `question`;
DROP TABLE IF EXISTS  `module`;
DROP TABLE IF EXISTS  `experience`;
DROP TABLE IF EXISTS  `enrollment`;
DROP TABLE IF EXISTS  `badge_user`;
DROP TABLE IF EXISTS  `badge`;
DROP TABLE IF EXISTS  `course`;
DROP TABLE IF EXISTS  `user`;

CREATE TABLE `user`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(150) NOT NULL,
  `role` VARCHAR(25) DEFAULT 'USER' check (role IN ('ADMIN','INSTRUCTOR','USER','SYSTEM')),
  `avatar` VARCHAR(1500),
  `date_of_birth` DATE NOT NULL,
  `address_number` VARCHAR(20),
  `address_street` VARCHAR(255),
  `address_city` VARCHAR(255),
  `address_postal_code` VARCHAR(10),
  `address_state` VARCHAR(50),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


CREATE TABLE  `experience`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `place` VARCHAR(50),
  `skill` VARCHAR(50),
  `user_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);



CREATE TABLE  `course`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `course_code` VARCHAR(25) NOT NULL UNIQUE,
  `course_name` VARCHAR(50) NOT NULL,
  `price` DECIMAL(5,2) NOT NULL,
  `description`  VARCHAR(500) NOT NULL,
  `image` VARCHAR(1000),
  `media` VARCHAR(1000),
  `language` VARCHAR(50) NOT NULL,
  `rating` FLOAT(1) NOT NULL,
  `skill_level` VARCHAR(20) NOT NULL,
  `total_mark` FLOAT(10) DEFAULT 0,
  `user_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);



CREATE TABLE  `requirement`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `description` VARCHAR(500) NOT NULL
);




CREATE TABLE  `course_requirement`(
  `course_id` VARCHAR(32) NOT NULL,
  `requirement_id` VARCHAR(32) NOT NULL,
  `skill_level` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`course_id`,`requirement_id`),
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  FOREIGN KEY (`requirement_id`) REFERENCES `requirement` (`id`)
);

CREATE TABLE  `module`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `module_name` VARCHAR(100) NOT NULL,
  `module_number` FLOAT(3) NOT NULL,
  `module_description` VARCHAR(500) NOT NULL,
  `media` VARCHAR(1000),
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);



CREATE TABLE  `question`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `question` VARCHAR(500) NOT NULL,
  `question_number` FLOAT(3) NOT NULL,
  `media` VARCHAR(1000),
  `mark` FLOAT(10) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);


CREATE TABLE  `answer`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `answer` VARCHAR(1000) NOT NULL,
  `is_right` BOOLEAN,
  `question_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
);



CREATE TABLE  `badge`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `badge_name` VARCHAR(50) NOT NULL,
  `badge_description` VARCHAR(500) NOT NULL,
  `badge_level` VARCHAR(25) NOT NULL check (badge_level IN ('EXPERT','ADVANCED','INTERMEDIATE','BEGINNER')),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);




CREATE TABLE  `badge_user`(
  `badge_id` VARCHAR(32) NOT NULL,
  `user_id` VARCHAR(32) NOT NULL,
  `assertion_id` VARCHAR(32) NOT NULL,
  `badge_url` VARCHAR(1000) NOT NULL,
  `qr_code` VARCHAR(1000),
  `badge_expired` TIMESTAMP,
  `badge_issue` TIMESTAMP NOT NULL,
  `badge_level` VARCHAR(100),
  `result` FLOAT(10),
  `hash` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`badge_id`, `user_id`),
  FOREIGN KEY (`badge_id`) REFERENCES `badge` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE  `enrollment`(
  `user_id` VARCHAR(32) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  `status` VARCHAR(25) DEFAULT 'NEW',
  `rating` FLOAT(1) DEFAULT 0,
  `user_total_mark` FLOAT(10) DEFAULT 0,
  `competency` VARCHAR(15) DEFAULT 'NOT CALIBRATED',
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`, `course_id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);

CREATE TABLE  `user_mark`(
  `user_id` VARCHAR(32) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  `question_id` VARCHAR(32) NOT NULL,
  `mark` FLOAT(10) DEFAULT 0,
  `answer` VARCHAR(1000) NOT NULL,
  `similarity` FLOAT(10) NOT NULL,
  `status` VARCHAR(25) DEFAULT 'NEW',
  PRIMARY KEY(`user_id`,`course_id`,`question_id`)
);


DROP TRIGGER IF EXISTS  `before_insert_user`;
DROP TRIGGER IF EXISTS  `before_insert_experience`;
DROP TRIGGER IF EXISTS  `before_insert_course`;
DROP TRIGGER IF EXISTS  `before_insert_requirement`;
DROP TRIGGER IF EXISTS  `before_insert_module`;
DROP TRIGGER IF EXISTS  `before_insert_question`;
DROP TRIGGER IF EXISTS  `before_insert_answer`;
DROP TRIGGER IF EXISTS  `before_insert_badge`;
DROP TRIGGER IF EXISTS  `before_insert_apostille_user`;


delimiter //
CREATE TRIGGER  before_insert_user
BEFORE INSERT ON  user
FOR EACH ROW
BEGIN
  IF NEW.id is NULL THEN
      SET NEW.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_experience`
BEFORE INSERT ON  `experience`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_course`
BEFORE INSERT ON  `course`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_requirement`
BEFORE INSERT ON  `requirement`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_module`
BEFORE INSERT ON  `module`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_question`
BEFORE INSERT ON  `question`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_answer`
BEFORE INSERT ON  `answer`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_badge`
BEFORE INSERT ON  `badge`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

DROP TRIGGER IF EXISTS  `after_update_user_mark`;

delimiter //
CREATE TRIGGER  `after_update_user_mark`
AFTER UPDATE ON  `user_mark`
FOR EACH ROW
BEGIN

    DECLARE number_question INT;
    DECLARE number_question_completed INT;

    SET number_question = (SELECT COUNT(*) FROM user_mark WHERE course_id = old.course_id AND user_id = old.user_id);
    SET number_question_completed = (SELECT COUNT(*) FROM user_mark WHERE course_id = old.course_id AND user_id = old.user_id AND status = 'COMPLETE');

    IF number_question = number_question_completed THEN
        UPDATE enrollment SET 
            status = 'COMPLETE'
        WHERE   
            user_id = old.user_id AND
            course_id = old.course_id;
    END IF;

END; //
delimiter ;

DROP TRIGGER IF EXISTS  `after_update_user_mark_totalmark`;

delimiter //
CREATE TRIGGER  `after_update_user_mark_totalmark`
AFTER UPDATE ON  `user_mark`
FOR EACH ROW
BEGIN

    DECLARE totalmark FLOAT(10);

    DECLARE done INT DEFAULT FALSE;
    DECLARE a VARCHAR(32);
    DECLARE cur1 CURSOR FOR SELECT mark FROM user_mark WHERE course_id = old.course_id AND user_id = old.user_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;
    SET totalmark = 0;

    read_loop: LOOP
        FETCH cur1 INTO a;

        IF done THEN
            LEAVE read_loop;
        END IF;
        
        SET totalmark = totalmark + a;

    END LOOP;
    
    UPDATE enrollment SET 
        user_total_mark = totalmark
    WHERE   
        user_id = old.user_id AND
        course_id = old.course_id;

    CLOSE cur1;


END; //
delimiter ;

DROP TRIGGER IF EXISTS  `after_insert_enrollment`;

delimiter //
CREATE TRIGGER  `after_insert_enrollment`
AFTER INSERT ON  `enrollment`
FOR EACH ROW
BEGIN

    DECLARE done INT DEFAULT FALSE;
    DECLARE a VARCHAR(32);
    DECLARE cur1 CURSOR FOR SELECT id FROM question WHERE course_id = new.course_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH cur1 INTO a;

        IF done THEN
            LEAVE read_loop;
        END IF;

        INSERT INTO user_mark(`user_id`,`course_id`,`question_id`) VALUES (new.user_id,new.course_id,a);

    END LOOP;

    CLOSE cur1;

END; //
delimiter ;

DROP TRIGGER IF EXISTS  `after_insert_question`;

delimiter //
CREATE TRIGGER  `after_insert_question`
AFTER INSERT ON  `question`
FOR EACH ROW
BEGIN

    DECLARE totalmark FLOAT(10);

    DECLARE done INT DEFAULT FALSE;
    DECLARE a VARCHAR(32);
    DECLARE cur1 CURSOR FOR SELECT mark FROM question WHERE course_id = new.course_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;
    SET totalmark = 0;

    read_loop: LOOP
        FETCH cur1 INTO a;

        IF done THEN
            LEAVE read_loop;
        END IF;
        
        SET totalmark = totalmark + a;

    END LOOP;
    
    UPDATE course SET 
        total_mark = totalmark
    WHERE   
        id = new.course_id;

    CLOSE cur1;


END; //
delimiter ;

DROP TRIGGER IF EXISTS  `after_insert_badge_user`;

delimiter //
CREATE TRIGGER  `after_insert_badge_user`
AFTER INSERT ON  `badge_user`
FOR EACH ROW
BEGIN

    DECLARE _id VARCHAR(32);

    SET _id = (SELECT course_id FROM badge WHERE id = new.badge_id);

    UPDATE enrollment SET 
        status = 'EVALUATED'
    WHERE   
        user_id = new.user_id AND
        course_id = _id;
        
END; //
delimiter ;


/* Admin */;
INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`,`role`) 
VALUES("0","U-Self","U-Self","01119545424","support@u-self.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00,"ADMIN");
/* System */;
INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`,`role`) 
VALUES("7","U-Self","U-Self","01119545424","system@u-self.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00,"SYSTEM");

/* Instructor */;
INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`,`role`) 
VALUES("1","Hafiz","Ghani","01119545424","muhamadhafiz.ghani@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00,"INSTRUCTOR");

/* Student */;
INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("2","Haikal","Kamal","01119545424","haikal.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("3","Jamal","Addin","01119545424","jamal.addin@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("4","Syahirah","Kamal","01119545424","Syahirah.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("5","Timah","Kamal","01119545424","timah.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("6","Haziq","Kamal","01119545424","haziq.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

/* Course Requirement */;

INSERT INTO `requirement`(`id`,`description`)
VALUES ("1", "Javascript Syntax");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("2", "React JS");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("3", "Material UI");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("4", "Computer Network");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("5", "Bootsrap");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("6", "Java");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("7", "Python");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("8", "MySQL");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("9", "AWS Account");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("10", "If you have a computer or a smartphone then you are ready to go!");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("11", "PC or Mac");


/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "1", 
        "Node JS For Beginner", 
        "NODEJS001",
        35.00,  
        "Dive deep under the hood of NodeJS. Learn V8, Express, the MEAN stack, core Javascript concepts, and more.",
        "https://en.wikipedia.org/wiki/Node.js#/media/File:Node.js_logo.svg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/1200px-Node.js_logo.svg.png",
        "English",
        "Beginner",
        "1", 5);
     /* Course Requirement*/   ;
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","4","Basic");
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","10","Mandatory");
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","11","Mandatory");
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","2","Basic");


    /* Module */;
    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("1","1",1,"Introduction to js","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","intro description");
        
    /* Module */;
    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("2","1",2,"Introduction CN","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","intro CN description");

    /* Module */;
    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("3","1",3,"Setup env","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","version 10 windows needed");

    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("4","1",4,"Setup tools","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","install node js, npm");

    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("5","1",5,"Development","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","For development in node");

    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("6","1",6,"Summary","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","rating us");

        
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("1",1,"1","What's the meaning of the javascript?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("1","1","C-Correct Answer",1);
    
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("2",2,"1","What's the second question?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("2","2","The second question answer",1);
    
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("3",3,"1","What's the meaning third question?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("3","3","The third question answer true ",1);

    
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("4",4,"1","What's the meaning of the javascript forth question?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("4","4","The second question answer",1);

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "2", 
        "Complete Ruby Guide", 
        "RUBY001",
        67.00,  
        "Learn to make innovative web apps with Ruby on Rails and unleash your creativity",
        "https://d8bwfgv5erevs.cloudfront.net/cdn/13/images/curso-online-ruby-on-rails_l_primaria_1_1520261687.jpg",
        "https://swiftlet.co.th/wp-content/uploads/2019/05/What-is-Ruby-on-Rails-1-1.png",
        "English",
        "Beginner",
        "1",4.5);
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","1","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","10","Mandatory");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","9","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","2","Basic");

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "3", 
        "AWS Certified Solutions", 
        "AWS001",
        120.00,  
        "390 AWS Certified Solutions Architect Associate Practice Test Questions in 6 sets w/ Complete Explanations & References",
        "https://cloudastronautblog.files.wordpress.com/2017/10/aws_logo_smile_1200x630.png?w=1400",
        "https://www.logicata.com/wp-content/uploads/2019/10/aws-300x169.png",
        "English",
        "Intermediate",
        "1",5);
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","1","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","10","Mandatory");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","9","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","2","Basic");

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "4", 
        "React Native: Advanced Concepts", 
        "REACT001",
        150.00,  
        "Master the advanced topics of React Native: Animations, Maps, Notifications, Navigation and More!",
        "https://cdn2.iconfinder.com/data/icons/designer-skills/128/react-512.png",
        "https://pbs.twimg.com/media/Dvg4Cb2XcAA77VA.png:large",
        "English",
        "Advanced",
        "1",4.6);
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","2","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","5","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","3","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","7","Basic");

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`rating`,`skill_level`,`user_id`)
VALUES( "5", 
        "Advanced Ruby on RAils Guide", 
        "RUBY021",
        67.00,  
        "Learn to make innovative web apps with Ruby on Rails and unleash your creativity with advanced tips from professional 2",
        "https://d8bwfgv5erevs.cloudfront.net/cdn/13/images/curso-online-ruby-on-rails_l_primaria_1_1520261687.jpg",
        "https://swiftlet.co.th/wp-content/uploads/2019/05/What-is-Ruby-on-Rails-1-1.png",
        "Englisher",
        3,
        "Beginnerer",
        "1");

/* enrollment */;

INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","3");
INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","4");
INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","1");
INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","5");

/* Badge */;
INSERT INTO `badge`(`id`,`badge_name`,`badge_level`,`badge_description`,`course_id`)
VALUES("_LKXdYWFSkalgiFeMN8scg","Node JS Expert","EXPERT","Awarded to recipient who enroll node js course which get 80 to 100 in Node JS assessment","1" );

INSERT INTO `badge`(`id`,`badge_name`,`badge_level`,`badge_description`,`course_id`)
VALUES("8ngM_fTASziR18JLie0ruA","Node JS Beginner","BEGINNER","Awarded to recipient who enroll node js course which get below than 50 in Node JS assessment","1" );

INSERT INTO `badge`(`id`,`badge_name`,`badge_level`,`badge_description`,`course_id`)
VALUES("AysIh41VQWiNPzXWcGs7Kw","Node JS Intermediate","INTERMEDIATE","Awarded to recipient who enroll node js course which get 50 to 64 in Node JS assessment","1" );

INSERT INTO `badge`(`id`,`badge_name`,`badge_level`,`badge_description`,`course_id`)
VALUES("eicrY9pZTF-HouYHchctGg","Node JS Advanced","ADVANCED","Awarded to recipient who enroll node js course which get 65 to 79 in Node JS assessment","1" );