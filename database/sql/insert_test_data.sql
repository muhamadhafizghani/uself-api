
/* Admin */;
INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`,`role`) 
VALUES("0","U-Self","U-Self","01119545424","support@u-self.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00,"ADMIN");

/* Instructor */;
INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`,`role`) 
VALUES("1","Hafiz","Ghani","01119545424","muhamadhafiz.ghani@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00,"INSTRUCTOR");

/* Student */;
INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("2","Haikal","Kamal","01119545424","haikal.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("3","Jamal","Addin","01119545424","jamal.addin@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("4","Syahirah","Kamal","01119545424","Syahirah.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("5","Timah","Kamal","01119545424","timah.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

INSERT INTO `user`(`id`,`first_name`,`last_name`,`phone`,`email`,`password`,`date_of_birth`) 
VALUES("6","Haziq","Kamal","01119545424","haziq.kamal@gmail.com",
"bd9e086bad87162201483faf96421b80a4704df9032aead8820b9539c1ee79bc5d49ac2d3fc852e43b78bb56339265e2e71e63128636e2dad3ca1b6a5815c4f4ce2cd6bce5844f89",0000-00-00);

/* Course Requirement */;

INSERT INTO `requirement`(`id`,`description`)
VALUES ("1", "Javascript Syntax");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("2", "React JS");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("3", "Material UI");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("4", "Computer Network");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("5", "Bootsrap");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("6", "Java");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("7", "Python");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("8", "MySQL");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("9", "AWS Account");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("10", "If you have a computer or a smartphone then you are ready to go!");

INSERT INTO `requirement`(`id`,`description`)
VALUES ("11", "PC or Mac");


/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "1", 
        "Node JS For Beginner", 
        "NODEJS001",
        35.00,  
        "Dive deep under the hood of NodeJS. Learn V8, Express, the MEAN stack, core Javascript concepts, and more.",
        "https://en.wikipedia.org/wiki/Node.js#/media/File:Node.js_logo.svg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/1200px-Node.js_logo.svg.png",
        "English",
        "Beginner",
        "1", 5);
     /* Course Requirement*/   ;
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","4","Basic");
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","10","Mandatory");
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","11","Mandatory");
    INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
    VALUES("1","2","Basic");


    /* Module */;
    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("1","1",1,"Introduction to js","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","intro description");
        
    /* Module */;
    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("2","1",2,"Introduction CN","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","intro CN description");

    /* Module */;
    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("3","1",3,"Setup env","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","version 10 windows needed");

    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("4","1",4,"Setup tools","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","install node js, npm");

    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("5","1",5,"Development","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","For development in node");

    INSERT INTO `module`(`id`,`course_id`,`module_number`,`module_name`,`media`,`module_description`)
    VALUES("6","1",6,"Summary","https://apostille-badge.s3-ap-southeast-1.amazonaws.com/node_js_video_intro.mp4","rating us");

        
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("1",1,"1","What's the meaning of the javascript?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("1","1","C-Correct Answer",1);
    
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("2",2,"1","What's the second question?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("2","2","The second question answer",1);
    
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("3",3,"1","What's the meaning third question?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("3","3","The third question answer true ",1);

    
    /* Question */;
    INSERT INTO `question`(`id`,`question_number`,`course_id`,`question`,`media`,`mark`)
    VALUES("4",4,"1","What's the meaning of the javascript forth question?","https://image",3);
        /* Answer */;
        INSERT INTO `answer`(`question_id`,`id`,`answer`,`is_right`)
        VALUES("4","4","The second question answer",1);

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "2", 
        "Complete Ruby Guide", 
        "RUBY001",
        67.00,  
        "Learn to make innovative web apps with Ruby on Rails and unleash your creativity",
        "https://d8bwfgv5erevs.cloudfront.net/cdn/13/images/curso-online-ruby-on-rails_l_primaria_1_1520261687.jpg",
        "https://swiftlet.co.th/wp-content/uploads/2019/05/What-is-Ruby-on-Rails-1-1.png",
        "English",
        "Beginner",
        "1",4.5);
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","1","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","10","Mandatory");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","9","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("2","2","Basic");

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "3", 
        "AWS Certified Solutions", 
        "AWS001",
        120.00,  
        "390 AWS Certified Solutions Architect Associate Practice Test Questions in 6 sets w/ Complete Explanations & References",
        "https://cloudastronautblog.files.wordpress.com/2017/10/aws_logo_smile_1200x630.png?w=1400",
        "https://www.logicata.com/wp-content/uploads/2019/10/aws-300x169.png",
        "English",
        "Intermediate",
        "1",5);
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","1","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","10","Mandatory");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","9","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("3","2","Basic");

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`skill_level`,`user_id`,`rating`)
VALUES( "4", 
        "React Native: Advanced Concepts", 
        "REACT001",
        150.00,  
        "Master the advanced topics of React Native: Animations, Maps, Notifications, Navigation and More!",
        "https://cdn2.iconfinder.com/data/icons/designer-skills/128/react-512.png",
        "https://pbs.twimg.com/media/Dvg4Cb2XcAA77VA.png:large",
        "English",
        "Advanced",
        "1",4.6);
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","2","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","5","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","3","Basic");
INSERT INTO `course_requirement`(`course_id`,`requirement_id`,`skill_level`)
VALUES("4","7","Basic");

/* Course */;
INSERT INTO `course`(`id`, `course_name`,`course_code`, `price`, `description`, `media`,`image`, `language`,`rating`,`skill_level`,`user_id`)
VALUES( "5", 
        "Advanced Ruby on RAils Guide", 
        "RUBY021",
        67.00,  
        "Learn to make innovative web apps with Ruby on Rails and unleash your creativity with advanced tips from professional 2",
        "https://d8bwfgv5erevs.cloudfront.net/cdn/13/images/curso-online-ruby-on-rails_l_primaria_1_1520261687.jpg",
        "https://swiftlet.co.th/wp-content/uploads/2019/05/What-is-Ruby-on-Rails-1-1.png",
        "Englisher",
        3,
        "Beginnerer",
        "1");

/* enrollment */;

INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","3");
INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","4");
INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","1");
INSERT INTO `enrollment`(`user_id`,`course_id`) 
VALUES("2","5");

/* Badge */;
INSERT INTO `badge`(`id`,`badge_name`,`badge_code`,`badge_description`,`reference_number`,`course_id`)
VALUES("1","Expert Node JS","JSEXPERT01","This badge will be issued to student who got 80% and above","1111","1" );
