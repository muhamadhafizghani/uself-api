
DROP TABLE IF EXISTS  `badge_recipient`;
DROP TABLE IF EXISTS  `apostille_badge`;
DROP TABLE IF EXISTS  `apostille_user`;


CREATE TABLE `apostille_user`(
  `issuer_id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `company_name` VARCHAR(50) NOT NULL UNIQUE,
  `image` VARCHAR(1000),
  `description` VARCHAR(500),
  `company_registration_number` VARCHAR(50) NOT NULL UNIQUE,
  `url` VARCHAR(1000) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(150) NOT NULL,
  `role` VARCHAR(25) DEFAULT 'USER' check (role IN ('ADMIN','USER')),
  `date_start_operate` DATE NOT NULL,
  `address_number` VARCHAR(20),
  `address_street` VARCHAR(255),
  `address_city` VARCHAR(255),
  `address_postal_code` VARCHAR(10),
  `address_state` VARCHAR(50),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `apostille_badge`(
  `badgeclass_id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `issuer_id` VARCHAR(32) NOT NULL,
  `badge_name` VARCHAR(50) NOT NULL,
  `badge_description` VARCHAR(500) NOT NULL,
  `image` VARCHAR(1000),
  `badge_url` VARCHAR(1000) NOT NULL,
  FOREIGN KEY (`issuer_id`) REFERENCES `apostille_user` (`issuer_id`),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


CREATE TABLE `badge_recipient`(
  `assertion_id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `badgeclass_id` VARCHAR(32) NOT NULL,
  `hash` VARCHAR(100) NOT NULL,
  `qr_code_url` VARCHAR(1000) NOT NULL,
  `badge_image_url` VARCHAR(1000) NOT NULL,
  `badge_image_apostille` VARCHAR(1000) NOT NULL,
  `recipient_name` VARCHAR(100) NOT NULL,
  `recipient_email` VARCHAR(255) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `url_evidence` VARCHAR(1000),
  FOREIGN KEY (`badgeclass_id`) REFERENCES `apostille_badge` (`badgeclass_id`),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

DROP FUNCTION IF EXISTS  `write_badge_recipient`;


delimiter //

CREATE FUNCTION write_badge_recipient ( 
    id VARCHAR(32),   
    badgeclass_id VARCHAR(32), 
    hash VARCHAR(100),
    badge_image_url VARCHAR(1000),
    badge_image_apostille VARCHAR(1000),
    recipient_name VARCHAR(100),
    recipient_email VARCHAR(255),
    description VARCHAR(500),
    url_evidence VARCHAR(1000),
    qr_code_url VARCHAR(1000),
    time_created TIMESTAMP )

RETURNS VARCHAR(32)

BEGIN

    DECLARE newId VARCHAR(32);
  IF id IS NULL THEN
    SET id = replace(uuid(),'-','');
  END IF;
    SET newId =id;

    INSERT INTO `badge_recipient`(  `assertion_id`,
                                    `badgeclass_id`,
                                    `hash`,
                                    `badge_image_url`,
                                    `badge_image_apostille`,
                                    `recipient_name` ,
                                    `recipient_email` ,
                                    `description` ,
                                    `url_evidence`,
                                    `qr_code_url`,
                                    `time_created` )
    VALUES (
        id,
        badgeclass_id,
        hash,
        badge_image_url,
        badge_image_apostille,
        recipient_name,
        recipient_email,
        description,
        url_evidence,
        qr_code_url,
        time_created  
    );

    RETURN newId;
END; //

delimiter ;

DROP FUNCTION IF EXISTS  `write_apostille_badge`;


delimiter //

CREATE FUNCTION write_apostille_badge ( 
    id VARCHAR(32),    
    issuer_id VARCHAR(32),
    badge_name VARCHAR(50),
    badge_description VARCHAR(500),
    image VARCHAR(1000),
    badge_url VARCHAR(1000))

RETURNS VARCHAR(32)

BEGIN

    DECLARE newId VARCHAR(32);
  IF id IS NULL THEN
    SET id = replace(uuid(),'-','');
  END IF;
    SET newId =id;

    INSERT INTO `apostille_badge`(  `badgeclass_id`,
                                    `issuer_id`,
                                    `badge_name`,
                                    `badge_description`,
                                    `image`,
                                    `badge_url` )
    VALUES (
        id,    
        issuer_id,
        badge_name,
        badge_description,
        image,
        badge_url 
    );

    RETURN newId;
END; //

delimiter ;