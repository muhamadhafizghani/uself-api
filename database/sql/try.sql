-- INSERT INTO `badge_user`(`badge_id`,`user_id`,`assertion_id`,`badge_url`,`badge_issue`,`badge_level`,`result`,`hash`,`qr_code`)
-- VALUES('_LKXdYWFSkalgiFeMN8scg','1','rXzXQPRHQtOwrIh37PWWDJ',
-- 'https://apostille-badge.s3-ap-southeast-1.amazonaws.com/ebadge/assertion-rXzXQPRHQtOwrIh37PWWDA -- Apostille TX c6863283abf5c30cfdcf787684563dff133dc4c0d64b2d3603ae78b47786f9aa -- Date 25-01-2020.png',
-- '2020-01-25 16:47:29','BEGINNER',49,'c6863283abf5c30cfdcf787684563dff133dc4c0d64b2d3603ae78b47786f9aa',
-- 'https://apostille-badge.s3-ap-southeast-1.amazonaws.com/qr/qr-rXzXQPRHQtOwrIh37PWWDA.png');



SELECT  U.badge_level

FROM
    enrollment E
LEFT JOIN course C ON C.id = E.course_id
LEFT JOIN badge B ON B.course_id = C.id
LEFT JOIN badge_user U ON B.user_id = E.user_id AND U.badge_id = B.id
WHERE 
    E.user_id = 1 AND E.course_id = 1
