DROP TABLE IF EXISTS  `answer`;
DROP TABLE IF EXISTS  `user_mark`;
DROP TABLE IF EXISTS  `course_requirement`;
DROP TABLE IF EXISTS  `requirement`;
DROP TABLE IF EXISTS  `question`;
DROP TABLE IF EXISTS  `module`;
DROP TABLE IF EXISTS  `experience`;
DROP TABLE IF EXISTS  `enrollment`;
DROP TABLE IF EXISTS  `badge_user`;
DROP TABLE IF EXISTS  `badge`;
DROP TABLE IF EXISTS  `course`;
DROP TABLE IF EXISTS  `user`;

DROP TABLE IF EXISTS  `badge_recipient`;
DROP TABLE IF EXISTS  `apostille_badge`;
DROP TABLE IF EXISTS  `apostille_user`;
