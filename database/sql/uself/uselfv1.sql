
DROP TABLE IF EXISTS `uself`.`user`;
CREATE TABLE `uself`.`user`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(150) NOT NULL,
  `is_instructor` BOOLEAN NOT NULL DEFAULT 0,
  'avatar' VARCHAR(1500),
  `date_of_birth` DATE NOT NULL,
  `address_number` VARCHAR(20),
  `address_street` VARCHAR(255),
  `address_city` VARCHAR(255),
  `address_postal_code` VARCHAR(10),
  `address_state` VARCHAR(50),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


DROP TABLE IF EXISTS `uself`.`experience`;
CREATE TABLE `uself`.`experience`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `place` VARCHAR(50),
  `skill` VARCHAR(50),
  `user_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);



DROP TABLE IF EXISTS `uself`.`course`;
CREATE TABLE `uself`.`course`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `course_code` VARCHAR(25) NOT NULL UNIQUE,
  `course_name` VARCHAR(50) NOT NULL,
  `price` DECIMAL(5,2) NOT NULL,
  `description`  VARCHAR(500) NOT NULL,
  `image` VARCHAR(1000),
  `media` VARCHAR(1000),
  `language` VARCHAR(50) NOT NULL,
  `rating` FLOAT(1) NOT NULL,
  `skill_level` VARCHAR(20) NOT NULL,
  `user_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);



DROP TABLE IF EXISTS `uself`.`requirement`;
CREATE TABLE `uself`.`requirement`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `req_code` VARCHAR(25) NOT NULL UNIQUE,
  `description` VARCHAR(500) NOT NULL
);




DROP TABLE IF EXISTS `uself`.`course_requirement`;
CREATE TABLE `uself`.`course_requirement`(
  `course_id` VARCHAR(32) NOT NULL,
  `requirement_id` VARCHAR(32) NOT NULL,
  `skill_level` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`course_id`,`requirement_id`),
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  FOREIGN KEY (`requirement_id`) REFERENCES `requirement` (`id`)
);

DROP TABLE IF EXISTS `uself`.`module`;
CREATE TABLE `uself`.`module`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `module_name` VARCHAR(100) NOT NULL,
  `module_description` VARCHAR(500) NOT NULL,
  `media` VARCHAR(1000),
  `total_mark` FLOAT(10),
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);



DROP TABLE IF EXISTS `uself`.`question`;
CREATE TABLE `uself`.`question`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `question` VARCHAR(500) NOT NULL,
  `media` VARCHAR(1000),
  `mark` FLOAT(10) NOT NULL,
  `module_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`module_id`) REFERENCES `module` (`id`)
);


DROP TABLE IF EXISTS `uself`.`answer`;
CREATE TABLE `uself`.`answer`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `answer` VARCHAR(1000) NOT NULL,
  `is_right` BOOLEAN,
  `question_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
);



DROP TABLE IF EXISTS `uself`.`badge`;
CREATE TABLE `uself`.`badge`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `badge_name` VARCHAR(50) NOT NULL,
  `badge_code` VARCHAR(25) NOT NULL UNIQUE,
  `badge_description` VARCHAR(500) NOT NULL,
  `reference_number` VARCHAR(25) NOT NULL,
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY ('course_id') REFERENCES `course` (`id`)
);




DROP TABLE IF EXISTS `uself`.`badge_user`;
CREATE TABLE `uself`.`badge_user`(
  `badge_id` VARCHAR(32) NOT NULL,
  `user_id` VARCHAR(32) NOT NULL,
  `badge_url` VARCHAR(1000) NOT NULL,
  `badge_expired` TIMESTAMP,
  `badge_issue` TIMESTAMP NOT NULL,
  `competency` VARCHAR(100),
  PRIMARY KEY (`badge_id`, `user_id`),
  FOREIGN KEY (`badge_id`) REFERENCES `badge` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

DROP TABLE IF EXISTS `uself`.`enrollment`;
CREATE TABLE `uself`.`enrollment`(
  `user_id` VARCHAR(32) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  `status` VARCHAR(25) NOT NULL DEFAULT 'NEW',
  `rating` FLOAT(1) NOT NULL DEFAULT 0,
  `competency` VARCHAR(15) NOT NULL,
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`, `course_id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);

DROP TABLE IF EXISTS `uself`.`user_mark`;
CREATE TABLE `uself`.`user_mark`(
  `user_id` VARCHAR(32) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  `module_id` VARCHAR(32) NOT NULL,
  `question_id` VARCHAR(32) NOT NULL,
  `mark` FLOAT(10) NOT NULL DEFAULT 0,
  `answer` VARCHAR(1000) NOT NULL,
  PRIMARY KEY(`user_id`,`course_id`,`module_id`,`question_id`)
);




DELIMITER ;;
DROP TRIGGER IF EXISTS `uself`.`before_insert_user`;
CREATE TRIGGER `uself`.`before_insert_user`
BEFORE INSERT ON `uself`.`user`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

DROP TRIGGER IF EXISTS `uself`.`before_insert_experience`;
CREATE TRIGGER `uself`.`before_insert_experience`
BEFORE INSERT ON `uself`.`experience`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

DROP TRIGGER IF EXISTS `uself`.`before_insert_course`;
CREATE TRIGGER `uself`.`before_insert_course`
BEFORE INSERT ON `uself`.`course`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

DROP TRIGGER IF EXISTS `uself`.`before_insert_requirement`;
CREATE TRIGGER `uself`.`before_insert_requirement`
BEFORE INSERT ON `uself`.`requirement`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

DROP TRIGGER IF EXISTS `uself`.`before_insert_module`;
CREATE TRIGGER `uself`.`before_insert_module`
BEFORE INSERT ON `uself`.`module`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

DROP TRIGGER IF EXISTS `uself`.`before_insert_question`;
CREATE TRIGGER `uself`.`before_insert_question`
BEFORE INSERT ON `uself`.`question`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

DROP TRIGGER IF EXISTS `uself`.`before_insert_answer`;
CREATE TRIGGER `uself`.`before_insert_answer`
BEFORE INSERT ON `uself`.`answer`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

DROP TRIGGER IF EXISTS `uself`.`before_insert_badge`;
CREATE TRIGGER `uself`.`before_insert_badge`
BEFORE INSERT ON `uself`.`badge`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END;

;;


INSERT INTO `uself`.`user`(`first_name`,`last_name`,`phone`,`email`,`date_of_birth`) VALUES("Hafiz","Ghani","01119545424","muhamadhafiz.ghani@gmail.com",0000-00-00);
