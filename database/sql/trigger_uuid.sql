DROP TRIGGER IF EXISTS  `before_insert_user`;
DROP TRIGGER IF EXISTS  `before_insert_experience`;
DROP TRIGGER IF EXISTS  `before_insert_course`;
DROP TRIGGER IF EXISTS  `before_insert_requirement`;
DROP TRIGGER IF EXISTS  `before_insert_module`;
DROP TRIGGER IF EXISTS  `before_insert_question`;
DROP TRIGGER IF EXISTS  `before_insert_answer`;
DROP TRIGGER IF EXISTS  `before_insert_badge`;
-- DROP TRIGGER IF EXISTS  `before_insert_apostille_user`;
-- DROP TRIGGER IF EXISTS  `before_insert_apostille_badge`;
-- DROP TRIGGER IF EXISTS  `before_insert_badge_recipient`;




delimiter //
CREATE TRIGGER  before_insert_user
BEFORE INSERT ON  user
FOR EACH ROW
BEGIN
  IF NEW.id is NULL THEN
      SET NEW.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_experience`
BEFORE INSERT ON  `experience`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_course`
BEFORE INSERT ON  `course`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_requirement`
BEFORE INSERT ON  `requirement`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_module`
BEFORE INSERT ON  `module`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_question`
BEFORE INSERT ON  `question`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_answer`
BEFORE INSERT ON  `answer`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;

delimiter //
CREATE TRIGGER  `before_insert_badge`
BEFORE INSERT ON  `badge`
FOR EACH ROW
BEGIN
  IF new.id IS NULL THEN
    SET new.id = replace(uuid(),'-','');
  END IF;
END; //
delimiter ;



-- delimiter //
-- CREATE TRIGGER  `before_insert_apostille_user`
-- BEFORE INSERT ON  `apostille_user`
-- FOR EACH ROW
-- BEGIN
--   IF new.id IS NULL THEN
--     SET new.id = replace(uuid(),'-','');
--   END IF;
-- END; //
-- delimiter ;

-- delimiter //
-- CREATE TRIGGER  `before_insert_apostille_badge`
-- BEFORE INSERT ON  `apostille_badge`
-- FOR EACH ROW
-- BEGIN
--   IF new.id IS NULL THEN
--     SET new.id = replace(uuid(),'-','');
--   END IF;
-- END; //
-- delimiter ;

-- delimiter //
-- CREATE TRIGGER  `before_insert_badge_recipient`
-- BEFORE INSERT ON  `badge_recipient`
-- FOR EACH ROW
-- BEGIN
--   IF new.id IS NULL THEN
--     SET new.id = replace(uuid(),'-','');
--   END IF;
-- END; //
-- delimiter ;