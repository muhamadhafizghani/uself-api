DROP TRIGGER IF EXISTS  `after_update_user_mark_totalmark`;

delimiter //
CREATE TRIGGER  `after_update_user_mark_totalmark`
AFTER UPDATE ON  `user_mark`
FOR EACH ROW
BEGIN

    DECLARE totalmark FLOAT(10);

    DECLARE done INT DEFAULT FALSE;
    DECLARE a VARCHAR(32);
    DECLARE cur1 CURSOR FOR SELECT mark FROM user_mark WHERE course_id = old.course_id AND user_id = old.user_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;
    SET totalmark = 0;

    read_loop: LOOP
        FETCH cur1 INTO a;

        IF done THEN
            LEAVE read_loop;
        END IF;
        
        SET totalmark = totalmark + a;

    END LOOP;
    
    UPDATE enrollment SET 
        user_total_mark = totalmark
    WHERE   
        user_id = old.user_id AND
        course_id = old.course_id;

    CLOSE cur1;


END; //
delimiter ;