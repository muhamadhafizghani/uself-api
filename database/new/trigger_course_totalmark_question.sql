DROP TRIGGER IF EXISTS  `after_insert_question`;

delimiter //
CREATE TRIGGER  `after_insert_question`
AFTER INSERT ON  `question`
FOR EACH ROW
BEGIN

    DECLARE totalmark FLOAT(10);

    DECLARE done INT DEFAULT FALSE;
    DECLARE a VARCHAR(32);
    DECLARE cur1 CURSOR FOR SELECT mark FROM question WHERE course_id = new.course_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;
    SET totalmark = 0;

    read_loop: LOOP
        FETCH cur1 INTO a;

        IF done THEN
            LEAVE read_loop;
        END IF;
        
        SET totalmark = totalmark + a;

    END LOOP;
    
    UPDATE course SET 
        total_mark = totalmark
    WHERE   
        id = new.course_id;

    CLOSE cur1;


END; //
delimiter ;