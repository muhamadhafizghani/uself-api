DROP TRIGGER IF EXISTS  `after_insert_enrollment`;

delimiter //
CREATE TRIGGER  `after_insert_enrollment`
AFTER INSERT ON  `enrollment`
FOR EACH ROW
BEGIN

    DECLARE done INT DEFAULT FALSE;
    DECLARE a VARCHAR(32);
    DECLARE cur1 CURSOR FOR SELECT id FROM question WHERE course_id = new.course_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur1;

    read_loop: LOOP
        FETCH cur1 INTO a;

        IF done THEN
            LEAVE read_loop;
        END IF;

        INSERT INTO user_mark(`user_id`,`course_id`,`question_id`) VALUES (new.user_id,new.course_id,a);

    END LOOP;

    CLOSE cur1;

END; //
delimiter ;