CREATE TABLE `user`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(150) NOT NULL,
  `role` VARCHAR(25) DEFAULT 'USER' check (role IN ('ADMIN','INSTRUCTOR','USER','SYSTEM')),
  `avatar` VARCHAR(1500),
  `date_of_birth` DATE NOT NULL,
  `address_number` VARCHAR(20),
  `address_street` VARCHAR(255),
  `address_city` VARCHAR(255),
  `address_postal_code` VARCHAR(10),
  `address_state` VARCHAR(50),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


CREATE TABLE  `experience`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `place` VARCHAR(50),
  `skill` VARCHAR(50),
  `user_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);



CREATE TABLE  `course`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `course_code` VARCHAR(25) NOT NULL UNIQUE,
  `course_name` VARCHAR(50) NOT NULL,
  `price` DECIMAL(5,2) NOT NULL,
  `description`  VARCHAR(500) NOT NULL,
  `image` VARCHAR(1000),
  `media` VARCHAR(1000),
  `language` VARCHAR(50) NOT NULL,
  `rating` FLOAT(1) NOT NULL,
  `skill_level` VARCHAR(20) NOT NULL,
  `total_mark` FLOAT(10) DEFAULT 0,
  `user_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);



CREATE TABLE  `requirement`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `description` VARCHAR(500) NOT NULL
);




CREATE TABLE  `course_requirement`(
  `course_id` VARCHAR(32) NOT NULL,
  `requirement_id` VARCHAR(32) NOT NULL,
  `skill_level` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`course_id`,`requirement_id`),
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  FOREIGN KEY (`requirement_id`) REFERENCES `requirement` (`id`)
);

CREATE TABLE  `module`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `module_name` VARCHAR(100) NOT NULL,
  `module_number` FLOAT(3) NOT NULL,
  `module_description` VARCHAR(500) NOT NULL,
  `media` VARCHAR(1000),
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);



CREATE TABLE  `question`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `question` VARCHAR(500) NOT NULL,
  `question_number` FLOAT(3) NOT NULL,
  `media` VARCHAR(1000),
  `mark` FLOAT(10) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);


CREATE TABLE  `answer`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `answer` VARCHAR(1000) NOT NULL,
  `is_right` BOOLEAN,
  `question_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
);



CREATE TABLE  `badge`(
  `id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `badge_name` VARCHAR(50) NOT NULL,
  `badge_description` VARCHAR(500) NOT NULL,
  `badge_level` VARCHAR(25) NOT NULL check (badge_level IN ('EXPERT','ADVANCED','INTERMEDIATE','BEGINNER')),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `course_id` VARCHAR(32) NOT NULL,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);




CREATE TABLE  `badge_user`(
  `badge_id` VARCHAR(32) NOT NULL,
  `user_id` VARCHAR(32) NOT NULL,
  `assertion_id` VARCHAR(32) NOT NULL,
  `badge_url` VARCHAR(1000) NOT NULL,
  `qr_code` VARCHAR(1000),
  `badge_expired` TIMESTAMP,
  `badge_issue` TIMESTAMP NOT NULL,
  `badge_level` VARCHAR(100),
  `result` FLOAT(10),
  `hash` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`badge_id`, `user_id`),
  FOREIGN KEY (`badge_id`) REFERENCES `badge` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE  `enrollment`(
  `user_id` VARCHAR(32) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  `status` VARCHAR(25) DEFAULT 'NEW',
  `rating` FLOAT(1) DEFAULT 0,
  `user_total_mark` FLOAT(10) DEFAULT 0,
  `competency` VARCHAR(15) DEFAULT 'NOT CALIBRATED',
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`, `course_id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
);

CREATE TABLE  `user_mark`(
  `user_id` VARCHAR(32) NOT NULL,
  `course_id` VARCHAR(32) NOT NULL,
  `question_id` VARCHAR(32) NOT NULL,
  `mark` FLOAT(10) DEFAULT 0,
  `answer` VARCHAR(1000) NOT NULL,
  `similarity` FLOAT(10) NOT NULL,
  `status` VARCHAR(25) DEFAULT 'NEW',
  PRIMARY KEY(`user_id`,`course_id`,`question_id`)
);




CREATE TABLE `apostille_user`(
  `issuer_id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `company_name` VARCHAR(50) NOT NULL UNIQUE,
  `image` VARCHAR(1000),
  `description` VARCHAR(500),
  `company_registration_number` VARCHAR(50) NOT NULL UNIQUE,
  `url` VARCHAR(1000) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(150) NOT NULL,
  `role` VARCHAR(25) DEFAULT 'USER' check (role IN ('ADMIN','USER')),
  `date_start_operate` DATE NOT NULL,
  `address_number` VARCHAR(20),
  `address_street` VARCHAR(255),
  `address_city` VARCHAR(255),
  `address_postal_code` VARCHAR(10),
  `address_state` VARCHAR(50),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE `apostille_badge`(
  `badgeclass_id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `issuer_id` VARCHAR(32) NOT NULL,
  `badge_name` VARCHAR(50) NOT NULL,
  `badge_description` VARCHAR(500) NOT NULL,
  `image` VARCHAR(1000),
  `badge_url` VARCHAR(1000) NOT NULL,
  FOREIGN KEY (`issuer_id`) REFERENCES `apostille_user` (`issuer_id`),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


CREATE TABLE `badge_recipient`(
  `assertion_id` VARCHAR(32) PRIMARY KEY NOT NULL,
  `badgeclass_id` VARCHAR(32) NOT NULL,
  `hash` VARCHAR(100) NOT NULL,
  `qr_code_url` VARCHAR(1000) NOT NULL,
  `badge_image_url` VARCHAR(1000) NOT NULL,
  `badge_image_apostille` VARCHAR(1000) NOT NULL,
  `recipient_name` VARCHAR(100) NOT NULL,
  `recipient_email` VARCHAR(255) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `url_evidence` VARCHAR(1000),
  FOREIGN KEY (`badgeclass_id`) REFERENCES `apostille_badge` (`badgeclass_id`),
  `time_created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `time_updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

