DROP TRIGGER IF EXISTS  `after_insert_badge_user`;

delimiter //
CREATE TRIGGER  `after_insert_badge_user`
AFTER INSERT ON  `badge_user`
FOR EACH ROW
BEGIN

    DECLARE _id VARCHAR(32);

    SET _id = (SELECT course_id FROM badge WHERE id = new.badge_id);

    UPDATE enrollment SET 
        status = 'EVALUATED'
    WHERE   
        user_id = new.user_id AND
        course_id = _id;
        
END; //
delimiter ;