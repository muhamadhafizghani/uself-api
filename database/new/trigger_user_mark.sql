DROP TRIGGER IF EXISTS  `after_update_user_mark`;

delimiter //
CREATE TRIGGER  `after_update_user_mark`
AFTER UPDATE ON  `user_mark`
FOR EACH ROW
BEGIN

    DECLARE number_question INT;
    DECLARE number_question_completed INT;

    SET number_question = (SELECT COUNT(*) FROM user_mark WHERE course_id = old.course_id AND user_id = old.user_id);
    SET number_question_completed = (SELECT COUNT(*) FROM user_mark WHERE course_id = old.course_id AND user_id = old.user_id AND status = 'COMPLETE');

    IF number_question = number_question_completed THEN
        UPDATE enrollment SET 
            status = 'COMPLETE'
        WHERE   
            user_id = old.user_id AND
            course_id = old.course_id;
    END IF;

END; //
delimiter ;